import subprocess
import time
import requests



class TablePowerManager:
    
    def __init__(self, url, table_client_args, table_server_args):
        self.url = url  #http://192.168.1.2:8080/devices/071e6a52-4a21-11e8-8a23-b827ebc3cb01
        self.table_client_args = table_client_args
        self.table_server_args = table_server_args
        self.table_client = None
        self.table_server = None
        self.table_controller = None
        self.t_on = 0.
    
    def get_status(self):
        r = requests.get(self.url)
        dico = r.json()
        return dico['attributes']['power']
    
    def get_on_time(self):
        return time.time() - self.t_on
    
    def switch(self, state):
        if self.get_status() != state:
            if state:
                r = requests.get(self.url + "/on")
            else:
                r = requests.get(self.url + "/off")
            time.sleep(5)
            return (self.get_status() == state)
        return True
    
    def shutdown(self):
        try:
            if self.switch(False):
                if self.table_client:
                    self.table_client.terminate()
                    self.table_client = None
                if self.table_server:
                    self.table_server.terminate()
                    self.table_server = None
                return True
        except Exception:
            pass
        return False
    
    def power(self, dt=30):
        try:
            if self.switch(True):
                self.t_on = time.time()
                time.sleep(dt)
                self.table_server = self.__class__.open_program(self.table_server_args)
                time.sleep(dt)
                self.table_client = self.__class__.open_program(self.table_client_args)
                time.sleep(dt)
                return True
        except Exception:
            pass
        return False
    
    @classmethod
    def open_program(cls, args=[]):
        return subprocess.Popen(args)




