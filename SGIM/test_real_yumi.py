import zmq
import time
import json
import copy

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')


class RealNode:
    
    def __init__(self):
        self.server = None
        self.client = None
        self.order = None
        self.working = True
        self.n = 0
        self.logger = ""
    
    def connect(self):
        ctx = zmq.Context()
        self.server = ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5555")
        self.client = ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5551")
    
    def move(self):
        print("Moving!")
        time.sleep(5)
        self.n += 1
    
    def reset(self):
        print("Resetting!")
        time.sleep(0.1 + self.n * 3)
        self.n = 0
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid"))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            order = json.loads(msg)
            return (order.has_key("type") and self.order.has_key("valid"))
        except Exception:
            return False
    
    def applyOrder(self):
        if self.order['type'] == "move" and self.order['payload'].has_key('path') and self.order['payload'].has_key('dt'):
            self.move()
        elif self.order['type'] == "reset" and self.order['payload'].has_key('dt') and self.order['payload'].has_key('dsNumerator') and self.order['payload'].has_key('dsDenominator'):
            self.reset()
    
    def check_state(self):
        ack = {'type': self.order['type'], 'from': 4}
        if self.order['type'] == "move":
            ack['valid'] = self.working
            ack['payload'] = {'joints': [0., 0., 0., 0., 0., 0., 0.]}
        elif self.order['type'] == "reset":
            ack['valid'] = self.working
        self.logger += "Sending: " + str(ack) + "\n"
        self.client.send(json.dumps(ack))
        msg = self.client.recv()
        self.logger += "Ack: " + msg + "\n"
        if not self.analyze_ack(msg):
            self.logger += "Coordinator node indicates an error...\n"
            print(self.logger)
            self.logger = ""
    
    def run(self):
        while True:
            try:
                msg = self.server.recv()
                self.logger += "Received order: " + msg + "\n"
                valid = self.analyze(msg)
                if not valid:
                    ack = {"type": "error", "payload": msg, "from": 4}
                    self.logger += "Acking: " + str(ack) + "\n"
                    self.server.send(json.dumps(ack))
                    print(self.logger)
                    self.logger = ""
                else:
                    ack = {"type": self.order['type'], "valid": True, "from": 4}
                    self.logger += "Acking: " + str(ack) + "\n"
                    self.server.send(json.dumps(ack))
                    self.applyOrder()
                    self.check_state()
            except Exception:
                self.logger += "Connexion error!\n"
                print(self.logger)
                self.logger = ""
                return
            self.logger = ""


if __name__ == "__main__":
    node = RealNode()
    node.connect()
    node.run()







