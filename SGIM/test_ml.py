import zmq
import time
import json
import copy

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')



class MLNode:
    
    def __init__(self):
        self.ctx = None
        self.server = None
        self.client = None
        self.order = None
        self.result = None
        self.logger = ""
        self.i = 0
    
    def connect(self):
        self.ctx = zmq.Context()
        self.server = self.ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5552")
        self.client = self.ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5551")
    
    def connectToReal(self):
        self.ctx = zmq.Context()
        self.server = self.ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5551")
        self.client = self.ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5555")
    
    def connectToSimu(self):
        self.ctx = zmq.Context()
        self.server = self.ctx.socket(zmq.REP)
        self.server.bind("tcp://*:5551")
        self.client = self.ctx.socket(zmq.REQ)
        self.client.connect("tcp://127.0.0.1:5554")
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid") and self.order.has_key("payload"))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            order = json.loads(msg)
            return (order.has_key("type") and order.has_key("valid"))
        except Exception:
            return False
    
    def moveOrder(self, path, ts):
        return {'type': 'move', 'valid': True, 'payload': {'path': path, "dt": ts}, 'from': 1}
    
    def resetOrder(self, joints):
        return {'type': 'reset', 'valid': True, 'payload': {'joints': joints, 'dt': 0.01, 'dsNumerator': 1, 'dsDenominator': 10}, 'from': 1}
    
    def sendOrder(self, order):
        self.client.send(json.dumps(order))
        msg = self.client.recv()
        self.logger += "Ack: " + msg + "\n"
        return self.analyze_ack(msg)
    
    def waitForAnswer(self):
        msg = self.server.recv()
        self.logger += "Received: " + msg + "\n"
        res = self.analyze(msg)
        if res:
            ack = {"type": self.order['type'], "from": 1, "valid": True}
        else:
            ack = {"type": "error", "from": 1, "valid": False}
        self.logger += "Acking: " + str(ack) + "\n"
        self.server.send(json.dumps(ack))
        return res
    
    def handle(self, order):
        self.logger += "Sending order: " + str(order) + "\n"
        if self.sendOrder(order):
            return self.waitForAnswer()
        else:
            return False
    
    def sendMove(self, path, dt):
        self.i += 1
        if self.handle(self.moveOrder(path, dt)) and self.order['payload'].has_key('y_types') \
                and self.order['payload'].has_key('y_list') and self.order['payload'].has_key('joints'):
            self.result = self.order['payload']
            return True
        else:
            return False
    
    def sendReset(self, joints):
        self.i += 1
        if self.handle(self.resetOrder(joints)) and self.order['payload'].has_key('y_types') \
                and self.order['payload'].has_key('y_list'):
            self.result = self.order['payload']
            return True
        else:
            return False


class MLNodeV2(MLNode):
    
    def __init__(self):
        MLNode.__init__(self)
        self.client2 = None   # table manager
    
    def connect(self):
        MLNode.connect(self)
        self.client2 = self.ctx.socket(zmq.REQ)
        self.client2.connect("tcp://127.0.0.1:5553")
    
    def analyze(self, msg):
        try:
            self.order = json.loads(msg)
            return (self.order.has_key("type") and self.order.has_key("valid"))
        except Exception:
            return False
    
    def analyze_ack(self, msg):
        try:
            self.ack = json.loads(msg)
            return (self.ack.has_key("type") and self.ack.has_key("valid"))
        except Exception:
            return False
    
    def analyze_table(self, msg):
        try:
            order = json.loads(msg)
            if (order.has_key("type") and order.has_key("payload") and order['payload'].has_key('y_list') \
                    and order['payload'].has_key('y_types')):
                return order['payload']['y_list'], order['payload']['y_types']
            self.logger += "An error occured with the table controller...\n"
            return [], []
        except Exception:
            self.logger += "An error occured with the table controller...\n"
            return [], []
    
    def sendMove(self, path, dt):
        self.i += 1
        return self.handle(self.moveOrder(path, dt))
    
    def sendReset(self, joints):
        self.i += 1
        # In this case the result is stored in self.order
        return self.handle(self.resetOrder(joints))
    
    def sendResetEnv(self):
        order = {"type": "reset", "from": 1}
        self.client2.send(json.dumps(order))
        msg = self.client2.recv()
    
    def sendOutcomes(self):
        order = {"type": "outcomes", "from": 1}
        self.client2.send(json.dumps(order))
        msg = self.client2.recv()
        return self.analyze_table(msg)
    
    def sendStop(self):
        order = {"type": "stop", "from": 1}
        self.client2.send(json.dumps(order))
        msg = self.client2.recv()
        return (self.analyze_ack(msg) and self.ack['valid'])
    
    def sendResume(self):
        order = {"type": "resume", "from": 1}
        self.client2.send(json.dumps(order))
        msg = self.client2.recv()
        return (self.analyze_ack(msg) and self.ack['valid'])



class MLNodeDemo(MLNodeV2):
    
    def connect(self):
        MLNode.connectToReal(self)
        self.client2 = self.ctx.socket(zmq.REQ)
        self.client2.connect("tcp://192.168.1.12:5559")
    
    def sendState(self, state):
        order = {"type": "state", "from": 1, "payload": state}
        self.client2.send(json.dumps(order))
        msg = self.client2.recv()
        return (self.analyze_ack(msg) and self.ack['valid'])




if __name__ == "__main__":
    node = MLNode()
    node.connect()
    
    # Add tests
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[1., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[2., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[3., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[4., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[-3., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[5., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.moveOrder([[6., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    raw_input()
    node.handle(node.resetOrder([0., 0., 0., 0., 0., 0., 0.]))
    
    raw_input()
    node.handle(node.moveOrder([[7., 0., 0., 0., 0., 0., 0.]], 0.02))
    
    
    
    
    
    
    
    
    
