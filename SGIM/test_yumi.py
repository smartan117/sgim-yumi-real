from utils import load_raw
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from yumi_control import SimuYumiExperimentator


dico = load_raw("A4_demos.raw")

n_demos = 0
for k1 in dico.keys():
    n_demos += len(dico[k1])

print("It contains the results of " + str(n_demos) + " demonstrations")

n_rem = 0
for k1 in dico.keys():
    for k2 in dico[k1].keys():
        if not dico[k1][k2][2]:
            del dico[k1][k2]
            n_rem += 1

print("Database contains " + str(n_rem) + " bad demonstrations")

plt.figure()

all_sound = []
exp = SimuYumiExperimentator()


for k1 in dico.keys():
    for k2 in dico[k1].keys():
        x = np.array([dico[k1][k2][0][0], dico[k1][k2][1][0]])
        y = np.array([dico[k1][k2][0][1], dico[k1][k2][1][1]])
        plt.plot(x, y, linestyle='solid', marker='o')
        """
        a = np.array(dico[k1][k2][0])
        b = np.array(dico[k1][k2][1])
        exp.env.simulator.compute_sound(a, b)
        sound = exp.env.simulator.sound
        #r = np.sqrt(np.sum((b-a)**2))
        """
        sound = dico[k1][k2][3]
        if sound[0]:
            all_sound.append(sound[1:])


print("The demonstrations made " + str(len(all_sound)) + " times some sound")
plt.show()

all_sound = np.array(all_sound)
print all_sound
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(all_sound[:,0], all_sound[:,1], all_sound[:,2])
ax.set_xlabel("frequency")
ax.set_ylabel("volume")
ax.set_zlabel("beat")
plt.show()