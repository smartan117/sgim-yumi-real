import zmq
import json
import numpy as np

from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.3f')

from yumi_control import Disk, TableReal
from table_manager import TablePowerManager



class TableController:
    
    def __init__(self, srvName, simulator, powerManager):
        self.srvName = srvName
        self.srv = None
        self.simulator = simulator
        self.powerManager = powerManager
        self.logger = ""
    
    def bind(self):
        ctx = zmq.Context()
        self.srv = ctx.socket(zmq.REP)
        #self.req.RCVTIMEO = 120000
        self.srv.bind(self.srvName)
        print("Server online!")
    
    def waitOrder(self):
        ack = {}
        msg = self.srv.recv()
        try:
            self.logger += "Received: " + msg + "\n"
            dico = json.loads(msg)
            #self.logger += "(type) " + str(dico['type']) + "\n"
            if dico['type'] == "pose" and dico.has_key('value'):
                # Message received by table
                print("Object at " + str(dico['value']))
                self.simulator.d_pose = np.array(dico['value'])
                ack = {'type': "pose", "valid": True}
            elif dico['type'] == "leave":
                # Message received by table
                self.simulator.d_pose = []
                print("Object left")
                ack = {'type': 'leave', 'valid': True}
            elif dico['type'] == "reset":
                # Setup must be reset
                self.simulator.reset()
                ack = {'type': 'reset', 'valid': True}
            elif dico['type'] == "outcomes":
                # Needs to compute outcomes and send them
                self.simulator.simulate()
                y_list, y_types = self.simulator.give_outcomes()
                ack = {'type': 'outcomes', 'payload': {'y_list': y_list, 'y_types': y_types}}
            elif dico['type'] == "stop":
                # Need to stop table and java table applications
                ack = {'type': 'stop', 'valid': self.powerManager.shutdown()}
                pass
            elif dico['type'] == "resume":
                # Need to launch table and java table applications
                ack = {'type': 'resume', 'valid': self.powerManager.power()}
                pass
            else:
                ack = {'type': 'error', 'payload': msg, 'valid': False}
                self.logger += "Acking: " + str(ack) + "\n"
                print(self.logger)
                self.logger = ""
        except Exception:
            self.logger += "Unknown message!\n"
            ack = {'type': 'error', 'payload': msg}
            print(self.logger)
            self.logger = ""
        self.logger += "Acking: " + str(ack) + "\n"
        self.srv.send(json.dumps(ack))
        self.logger = ""
        
        


class YumiExperimentSimulatorV3:
    
    def __init__(self, objects, table, r_thr=np.float("inf")):
        self.objects = objects
        self.d_pose = []
        self.grabbed = None
        self.table = table
        self.bad = False
        self.poses = []
        self.sound = [False, 0.0, 0.0, 0.0]   # Make sound, frequency, intensity, beat
        self.r_thr = r_thr
        for obj in self.objects:
            self.poses.append({"pose": obj.pose, "q": np.array([1, 0, 0, 0])})
        self.r_min = self.objects[0].r + self.objects[1].r
        self.A = np.log10(self.r_min)
        self.N = 2.0 / (np.log10(self.table.semi_diagonal * 2.0) - self.A)
        self.duration = -2.
    
    def reset(self):
        self.bad = False
        self.sound = [False, 0.0, 0.0, 0.0]
        self.grabbed = None
        self.duration = -2.
        for pose, obj in zip(self.poses, self.objects):
            obj.pose = pose["pose"]
            obj.detected = False
    
    def compute_sound(self, a, b):
        u = b - a
        r = np.sqrt(np.sum(u**2))
        rho = np.arccos(np.sum(u * np.array([1.0, 0.0])) / r)
        dist = []
        for p in self.table.corners:
            dist.append(np.sqrt(np.sum((p - a)**2)))
        self.sound[1] = (self.table.semi_diagonal/2.0 - min(dist)) * 2.0 / self.table.semi_diagonal
        self.sound[2] = 1.0 - 2.0 * (np.log10(r) - np.log10(self.r_min)) / (np.log10(self.table.semi_diagonal*2.0) - np.log10(self.r_min))
        self.sound[3] = (rho / np.pi) * 0.95 + 0.05
        self.sound[0] = (r <= self.r_thr)
    
    def simulate(self):
        if len(self.d_pose) > 0:
            n_pose = self.table.normalize_d(self.d_pose)
            pose = self.table.denormalize(n_pose)
            
            if self.grabbed:
                # An object was grabbed and must be put on table
                self.grabbed.pose = pose
                self.grabbed.detected = True
                ido = -1
                for i, obj in enumerate(self.objects):
                    if obj is self.grabbed:
                        ido = i
                    if obj is not self.grabbed and Disk.collision(self.grabbed, obj):
                        # A collision occured
                        self.bad = True
                print("Object " + str(ido) + " let go")
                self.grabbed = None
            else:
                # Must check if an object is near
                for i, obj in enumerate(self.objects):
                    if obj.inside(pose):
                        obj.detected = False
                        self.grabbed = obj
                        print("Object " + str(i) + " grabbed")
            if not self.bad and self.sound[0] and self.objects[0].detected and self.objects[1].detected:
                # A sound was created before, possible to modulate duration
                d = self.objects[0].pose[:2] - pose[:2]
                r = np.sqrt(np.sum(d**2))
                self.duration = (r - self.table.semi_diagonal) / self.table.semi_diagonal
            else:
                self.duration = -2.
            if not self.bad:
                if self.objects[0].detected and self.objects[1].detected:
                    self.compute_sound(self.objects[0].pose[:2], self.objects[1].pose[:2])
                else:
                    self.sound = [False, 0.0, 0.0, 0.0]
    
    def give_outcomes(self):
        y_list = []
        y_types = []
        if self.bad:
            return [], []
        if len(self.d_pose) > 0:
            # The hand is detected
            y_list.append(self.table.normalize_d(self.d_pose).tolist())
            y_types.append(0)
        both = []
        if self.objects[0].detected:
            obj1 = self.table.normalize(self.objects[0].pose).tolist()[:2]
            y_list.append(obj1)
            both += obj1
            y_types.append(1)
        if self.objects[1].detected:
            obj2 = self.table.normalize(self.objects[1].pose).tolist()[:2]
            y_list.append(obj2)
            both += obj2
            y_types.append(2)
        if len(both) == 4:
            y_list.append(both)
            y_types.append(3)
        if self.sound[0]:
            y_list.append(self.sound[1:])
            y_types.append(4)
            if self.duration >= -1.:
                y_list.append(self.sound[1:] + [self.duration])
                y_types.append(5)
        return y_list, y_types


def main():
    table = TableReal([[-292.5, -517.5],[292.5, 517.5]], [[0., 0.],[1., 585./1035.]])
    
    objA = Disk(40., np.array([-92.5, -250.]))
    objB = Disk(40., np.array([-142.5, 50.]))
    
    simu = YumiExperimentSimulatorV3([objA, objB], table, 300.)
    
    power_manager = TablePowerManager("http://192.168.1.2:8080/devices/071e6a52-4a21-11e8-8a23-b827ebc3cb01", \
            ["java", "-jar", "table.jar", "client-config.xml"], ["java", "-jar", "table.jar", "server-config.xml"])
    try:
        print("Table power: " + str(power_manager.get_status()))
    except Exception:
        print("Impossible to reach power manager")
        return
    
    server = TableController("tcp://127.0.0.1:5553", simu, power_manager)
    server.bind()
    if server.powerManager.power():
        print("Table and controllers are up.")
    else:
        print("An error occured while starting up table and controllers.")
        return

    while True:
        server.waitOrder()


if __name__ == "__main__":
    main()
    
