from utils import *
from yumi_control import *



eva1 = load_bin("Real_Yumi/SGIM_ACTS/eval_test_21900")
eva2 = load_bin("Real_Yumi/SGIM_PB/eval_test_21000")

data1 = eva1.learning_agent.dataset
data2 = eva2.learning_agent.dataset

# goal in \Omega_5 such as
# made by procedure by SGIM-PB and ideal action 4+1
# component in \Omega_4 doable by SGIM-ACTS

print("SGIM-ACTS outcomes in task 0/4/5: " + str(len(data1.y_spaces[0].data)) + "/" + str(len(data1.y_spaces[4].data)) + "/" + + str(len(data1.y_spaces[5].data)))
print("SGIM-PB outcomes in task 0/4/5: " + str(len(data2.y_spaces[0].data)) + "/" + str(len(data2.y_spaces[4].data)) + "/" + + str(len(data2.y_spaces[5].data)))


print data1.epmem_length[-50:]
