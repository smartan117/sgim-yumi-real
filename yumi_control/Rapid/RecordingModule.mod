MODULE RecordingModule
    !***********************************************************
    !
    ! Module:  MainSimuModule
    !
    ! Description:
    !   Skeleton module to control a simulated YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
	! Different constants used for motion control
	
	CONST num defaultSize := 1000;
	VAR robtarget trajectory{defaultSize};
    VAR jointtarget path{defaultSize};
	VAR num curSize := 0;
	PERS num timestep := 0.02;
	
	PERS bool startsync := FALSE;
	PERS bool stopsync := TRUE;
    
    PERS string recordPrefix := "trajectory";
	
	
	PROC main()
        WHILE TRUE DO
            Stop;
            IF IsFile(("HOME:/" + recordPrefix + "P.data")) THEN
                removeFile ("HOME:/" + recordPrefix + "P.data");
            ENDIF
            IF IsFile(("HOME:/" + recordPrefix + "J.data")) THEN
                removeFile ("HOME:/" + recordPrefix + "J.data");
            ENDIF
            curSize := 0;
    		WaitUntil startsync;
    		WHILE curSize < defaultSize AND (NOT stopsync) DO
    			curSize := curSize + 1;
    			trajectory{curSize} := CRobT(\TaskName:="T_ROB_R", \Tool:=VaccumOne);
                path{curSize} := CJointT();
    			WaitTime timestep;
    		ENDWHILE
            writePathMSimplified (recordPrefix + "P.data"), trajectory, \Size := curSize;
            writePathJSimplified (recordPrefix + "J.data"), path, \Size := curSize;
        ENDWHILE
	ENDPROC
    
ENDMODULE