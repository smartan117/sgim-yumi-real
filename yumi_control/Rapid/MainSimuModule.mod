MODULE MainSimuModule
    !***********************************************************
    !
    ! Module:  MainSimuModule
    !
    ! Description:
    !   Skeleton module to control a simulated YuMi robot
    !
    ! Author: Nicolas Duminy
    !
    ! Version: 1.0
    !
    !***********************************************************
	! Different constants used for motion control
	CONST num defaultSize := 1000;
	CONST zonedata passByZone := z10;    
	
	PERS num timestep := 0.02;
	VAR num timeSteps{defaultSize};
	VAR jointtarget path{defaultSize};
    VAR robtarget position{defaultSize};
    VAR bool under{defaultSize};
    VAR robtarget prePose;
	VAR robtarget finalPose;
    VAR jointtarget finalJoints;
	VAR num finalSize;
	VAR num curSize := 0;
	
	VAR shapedata volume;
    VAR shapedata slowArea;
    VAR shapedata slowJoint;
	CONST pos corner1 := [-1000,-1000,175]; !60
	CONST pos corner2 := [1000,1000,1000];
    CONST pos corner3 := [-1000,-1000,175];
    CONST pos corner4 := [1000,1000,275];
	VAR wztemporary forbidden_space;
    VAR wztemporary slow_space;
    VAR wztemporary slow_joint;
    VAR signaldo do_service;
	
	VAR num exec_status := 0;
	! 0 means reset
	! 1 means initialization complete
	! i means before move i
	
    PERS string pathFileName := "path.data";
    
	PERS bool startsync := FALSE;
	PERS bool stopsync := TRUE;
    VAR bool isReset := FALSE; 
    VAR num global_index := 0;
	VAR jointtarget initposition:= [[-134.36, 21.66, -8.05, 165.91, 32.73, -41.27], [131.37,9E9,9E9,9E9,9E9,9E9]];
    ! [[141.926,-91.9894,14.8817,146.197,57.2076,34.6444], [-52.1023,9E9,9E9,9E9,9E9,9E9]]
    VAR jointtarget calibposition:= [[0,-130,30,0,40,0], [-135,9E9,9E9,9E9,9E9,9E9]];
    VAR jointtarget low_pos := [[-168.5,-143.5,-123.5,-290,-88,-229], [-168.5,9E9,9E9,9E9,9E9,9E9]];
    VAR jointtarget high_pos := [[168.5,-80,80,290,138,229], [168.5,9E9,9E9,9E9,9E9,9E9]];
    VAR num exeTime := 0;
    
    VAR intnum slowHigh;
    VAR intnum slowLow;
    VAR num slowSpeed := 20;
    VAR num normalSpeed := 100;
    VAR num resetSpeed := 100;
    VAR num vMax := 1000;
    
    VAR jointtarget jointtest;
    VAR jointtarget jointtest_fin;
    VAR num test_val := 0;
    VAR num speedTest := 0;
    VAR num speedTestFin := 0;
    VAR num speedTest1 := 0;
    VAR num speedTest2 := 0;
    VAR jointtarget pathtest;
    VAR num timeExe := 0; 
    
	PROC main()
        !MoveAbsJ initposition, v1000, \T:=timestep, fine, VaccumOne;
        !MoveAbsJ calibposition, v1000, \T:=timestep, fine, VaccumOne;
        WaitTime 1;
        init; 
		exec_status := 1;        
		WHILE TRUE DO
			Stop;   
            prePose := CRobT(\Tool:=VaccumOne);
            readPathJSimplified pathFileName, path, position, finalSize;
            pathtest := path{2};
            !timeStepControl;
            speedSignalControl;
			easyPathExecute;
            IF IsFile(("HOME:/" + pathFileName)) THEN
                removeFile ("HOME:/" + pathFileName);  
            ENDIF
            !executePath;            
			exec_status := exec_status + 1;            
		ENDWHILE
	ENDPROC
	
	PROC init()
        IF IsFile(("HOME:/" + pathFileName)) THEN
            removeFile ("HOME:/" + pathFileName); 
        ENDIF
		! Declare world zones
		WZBoxDef \Outside, volume, corner1, corner2;
		WZLimSup \Temp, forbidden_space, volume;
		WZEnable forbidden_space;
        
        ! Declare box slow zones
        WZBoxDef \Inside, slowArea, corner3, corner4;
        WZDOSet \Temp, slow_space \Before, slowArea, custom_DO_1, 1;
        WZEnable slow_space;
        
        ! Declare joint slow zones
        WZLimJointDef \Inside, slowJoint, low_pos, high_pos;        
        WZDOSet \Temp, slow_joint \Before, slowJoint, custom_DO_1, 1;
        WZEnable slow_joint;       
        
        CONNECT slowHigh WITH moveSlow;
        CONNECT slowLow WITH moveNormal;
        ISignalDO custom_DO_1, 1, slowHigh;
        ISignalDO custom_DO_1, 0, slowLow;
        SpeedRefresh normalSpeed;
		RETURN;
	ENDPROC

    PROC gotoinitposition()
        MoveAbsJ initposition, v1000, \T:=timestep, fine, VaccumOne;    
        RETURN;
    ENDPROC
    
    PROC gotocalibposition()
        MoveAbsJ calibposition, v1000, \T:=timestep, fine, VaccumOne;    
        RETURN;
    ENDPROC
 
    PROC speedSignalControl()
        IF isReset THEN
            ISleep slowHigh;
            ISleep slowLow;
            SpeedRefresh resetSpeed;
        ELSE
            IWatch slowHigh;
            IWatch slowLow;
        ENDIF
    ENDPROC
    
	PROC executePath()
		VAR num index := 1;
		startsync := TRUE;
		stopsync := FALSE;
		WHILE index < finalSize - 1 DO
			IF curSize >= index THEN
				MoveAbsJ path{index}, v1000, \T:=timestep, passByZone, VaccumOne;
				index := index + 1;
			ENDIF
		ENDWHILE
		MoveAbsJ path{curSize}, v1000, \T:=timestep, fine, VaccumOne;
		finalPose := CRobT(\Tool:=VaccumOne);
        finalJoints := CJointT();
		startsync := FALSE;
		stopsync := TRUE;
        
	ENDPROC

    PROC timeStepControl()
        VAR num index := 1;
        VAR num x := 0;
        VAR num y := 0;
        VAR num z := 0;
        VAR num distance := 0;
        VAR num vPrdct := 0;
        
        x := prePose.Trans.X;
        y := prePose.Trans.Y;
        z := prePose.Trans.Z;
        FOR index FROM 1 TO finalSize DO
            distance := (x - position{index}.Trans.X) * (x - position{index}.Trans.X) + (y - position{index}.Trans.Y) * (y - position{index}.Trans.Y) + (z - position{index}.Trans.Z)*(z - position{index}.Trans.Z);
            distance := Sqrt(distance);
            vPrdct := distance / timestep;
            IF vPrdct > vMax THEN
                timeSteps{index} := distance / vMax;
            ELSE
                timeSteps{index} := timestep;
            ENDIF
            x := position{index}.Trans.X;
            y := position{index}.Trans.Y;
            z := position{index}.Trans.Z;
            IF z < 275 THEN
                under{index} := TRUE;
            ELSE
                under{index} := false;
            ENDIF
        ENDFOR
    ENDPROC
    
    PROC easyPathExecute()
        VAR clock exeClock;
        VAR num index := 1;        
		startsync := TRUE;
		stopsync := FALSE;
        speedTestFin := timeSteps{finalSize};
        speedTest2 := timeSteps{2};
        speedTest1 := timeSteps{1};        
        
        ClkReset execlock;
        ClkStart exeClock;
		WHILE index < finalSize and custom_DO_3 = 0 DO  ! Means No collision           
            global_index := index;
            IF custom_DO_1 = 1 or under{index} THEN ! In slow zone
                timeExe := timeSteps{index};
                MoveAbsJ path{index}, v1000, \T:=timeSteps{index}, passByZone, VaccumOne;
            ELSE
                timeExe := timeStep;
                MoveAbsJ path{index}, v1000, \T:=timeStep, passByZone, VaccumOne;
            ENDIF
            jointtest := path{index};
            speedTest := timeSteps{index};
            index := index + 1;
		ENDWHILE
        
        IF custom_DO_3 = 0 THEN ! Means No collision
            global_index := finalSize;
            jointtest_fin := path{finalSize};
            IF custom_DO_1 = 1 or under{index} THEN ! In slow zone
    		    MoveAbsJ path{finalSize}, v1000, \T:=timeSteps{finalSize}, fine, VaccumOne;
            ELSE
                MoveAbsJ path{finalSize}, v1000, \T:=timeStep, fine, VaccumOne;
            ENDIF             
        ENDIF            
        ClkStop exeClock;
        exeTime := ClkRead(exeClock);
        
		finalPose := CRobT(\Tool:=VaccumOne);
        finalJoints := CJointT();
		startsync := FALSE;
		stopsync := TRUE;
    ENDPROC
    
    TRAP moveSlow
        SpeedRefresh slowSpeed;
    ENDTRAP
    
    TRAP moveNormal
        SpeedRefresh normalSpeed;
    ENDTRAP
    
    PROC readPathJSimplified(string fileName, inout jointtarget data{*}, inout robtarget position{*}, inout num size)
        VAR iodev stream;
        VAR num index := 1;
        VAR string plop := "";
        VAR bool ok;
        VAR num line{7};
        VAR jointtarget j;
        VAR num found;
        VAR num len;
        Open "HOME:" \File:=fileName, stream, \Read;
        
        WHILE plop <> "EOF" and index <= Dim(data, 1) DO
            found := 0;
            plop := ReadStr(stream);
            found := StrMatch(plop,1,"\0D");
            plop := StrPart(plop,1,found - 1);        
            ok := StrToVal(plop, line);
            IF ok THEN !Eliminate EOF
                j.robax.rax_1 := line{1};
                j.robax.rax_2 := line{2};
                j.robax.rax_3 := line{3};
                j.robax.rax_4 := line{4};
                j.robax.rax_5 := line{5};
                j.robax.rax_6 := line{6};
                j.extax.eax_a := line{7};
                data{index} := j;
                position{index} := CalcRobT(j, VaccumOne);
                size := index;
                index := index + 1;
            ENDIF
        ENDWHILE
        Close stream;
    ENDPROC	
ENDMODULE