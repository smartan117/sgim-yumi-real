﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ABB.Robotics.Controllers;
using ABB.Robotics.Controllers.Discovery;
using ABB.Robotics.Controllers.RapidDomain;
using ABB.Robotics.Controllers.IOSystemDomain;
using System.Diagnostics;

namespace YumiControlPC
{
    public class YumiLowLevelController
    {

        private static int SIZE_MOTION = 30;
        private static int JOINT_PATH_ORDER = 1;
        private static int POSE_ORDER = 2;
        private static int JOINT_ORDER = 3;
        private static int POSE_PATH_ORDER = 4;
        private static int RECORD_ORDER = 5;
        private static int JOINT_ORDER2 = 6;
        private static int POSE_ORDER2 = 7;
        private static int GOTO_ORDER = 11;
        private static int GOTO_POSE_ORDER = 12;
        private static int JOINT_PATH_ORDER3 = 13;
        private static int STOP_ORDER = 42;
        private static int WINDOW = 20;

        //private NetworkScanner scanner = null;
        private Controller controller = null;
        private string task;
        private string module;

        public YumiLowLevelController(string task, string module)
        {
            this.task = task;
            this.module = module;
        }

        public bool connectRobot(ControllerInfo controllerInfo, string user = null, string pwd = null)
        {
            //scanner = new NetworkScanner();
            //scanner.Scan();
            //ControllerInfoCollection controllers = scanner.Controllers;

            //ControllerInfo controllerInfo = controllers[0];

            if
                (controllerInfo.Availability == ABB.Robotics.Controllers.Availability.Available)
            {
                if (controller != null)
                {
                    return false;
                }
                controller = ControllerFactory.CreateFrom(controllerInfo);
                if(user == null || pwd == null)
                    controller.Logon(UserInfo.DefaultUser);
                else
                {
                    UserInfo info = new UserInfo(user, pwd);
                    controller.Logon(info);
                }
                return (controller.OperatingMode == ControllerOperatingMode.Auto);
            }
            return false;
        }

        public bool disconnectRobot()
        {
            if (controller != null)
            {
                controller.Logoff();
                controller.Dispose();
                controller = null;
            }
            return true;
        }

        public Mastership requestMastership()
        {
            return Mastership.Request(controller.Rapid);
        }

        private RapidData synchronizeRapid(string task, string module, string varName)
        {
            return controller.Rapid.GetRapidData(task, module, varName);
        }

        public bool startTask()
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                using (requestMastership())
                {
                    StartResult res = controller.Rapid.Start(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
        }
        
        public bool stopTask()
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                using (requestMastership())
                {
                    controller.Rapid.Stop(StopMode.Immediate);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return (wTask.ExecutionStatus == TaskExecutionStatus.Stopped);
        }

        public bool isRunning()
        {
            Task wTask = controller.Rapid.GetTask(task);
            return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
        }

        public bool resetPointer()
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                using (requestMastership())
                {
                    wTask.ResetProgramPointer();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

        public bool setPointer(string routine, int line)
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                using (requestMastership())
                {
                    ProgramPosition pos = new ProgramPosition(module, routine, new TextRange(line));
                    wTask.SetProgramPointer(pos);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return false;
        }

        public bool readData<T>(string task, string module, string varName, ref T var)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is T)
            {
                var = (T) rd.Value;
                return true;
            }
            return false;
        }

        public bool writeData(string task, string module, string varName, IRapidData var)
        {
            bool success = false;
            try
            {
                using (this.requestMastership())
                {
                    RapidData rd = synchronizeRapid(task, module, varName);
                    if (rd.Value.GetType().IsAssignableFrom(var.GetType()))
                    {
                        rd.Value = var;
                        success = true;
                    }
                }                    
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool writeArrayData(string task, string module, string varName, IRapidData var, int i)
        {
            bool success = false;
            try
            {
                using (this.requestMastership())
                {
                    RapidData rd = synchronizeRapid(task, module, varName);
                    if (rd.Value is ArrayData)
                    {
                        rd.WriteItem(var, i);
                        success = true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool readArrayData<T>(string task, string module, string varName, ref T var, int i)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is ArrayData)
            {
                var = (T) rd.ReadItem(i);
                return true;
            }
            return false;
        }

        public bool writeArray(string task, string module, string varName, IRapidData[] var)
        {
            bool success = false;
            try
            {
                using (this.requestMastership())
                {
                    RapidData rd = synchronizeRapid(task, module, varName);

                    if (rd.Value is ArrayData)
                    {
                        for (int i = 0; i < var.Length; i++)
                        {
                            rd.WriteItem(var[i], i);
                        }
                    }
                }
                success = true;
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool writeSignalDO(string id, bool value)
        {
            bool write = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                //Signal must be set as accessible (access to ALL)
                if (value)
                    diSig1.Set();
                else
                    diSig1.Reset();
                write = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            return write;
        }

        public bool readSignalDO(string id, ref bool value)
        {
            bool read = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                value = diSig1.Get() != 0;
                read = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
            return read;
        }

        public bool triggerInterrupt(string id)
        {
            bool success = false;
            bool value = true;
            if (!readSignalDO(id, ref value))
                return false;
            if (value)
            {
                success = writeSignalDO(id, false);
                if (!success)
                    return false;
            }
            success = writeSignalDO(id, true);
            if (!success)
                return false;
            success = writeSignalDO(id, false);
            return success;
        }

        public bool sendOrder(int order)
        {
            Num val = default(Num);
            val.Value = order;
            return this.writeData(task, module, "askOrder", val);
        }

        public bool sendDuration(float duration)
        {
            Num val = default(Num);
            val.Value = duration;
            return writeData(task, module, "askDuration", val);
        }

        public bool sendTimestep(float dt)
        {
            Num val = default(Num);
            val.Value = dt;
            return writeData(task, module, "askTimestep", val);
        }

        public bool sendRecordOrder(float dt)
        {
            bool write = sendTimestep(dt);
            if (write)
                return sendOrder(RECORD_ORDER);
            return false;
        }

        public bool stopRecording()
        {
            Bool value = default(Bool);
            value.Value = false;
            return writeData(task, module, "recording", value);
        }

        public bool sendJointPath2(float[,] path, int end, int start = 0)
        {
            bool success = false;
            try
            {
                using (this.requestMastership())
                {
                    RapidData rd = synchronizeRapid(task, module, "askPathJ");

                    if (rd.Value is ArrayData)
                    {
                        JointTarget joints = default(JointTarget);
                        for (int i = start; i <= end; i++)
                        {
                            joints.RobAx.Rax_1 = path[i, 0];
                            joints.RobAx.Rax_2 = path[i, 1];
                            joints.RobAx.Rax_3 = path[i, 2];
                            joints.RobAx.Rax_4 = path[i, 3];
                            joints.RobAx.Rax_5 = path[i, 4];
                            joints.RobAx.Rax_6 = path[i, 5];
                            joints.ExtAx.Eax_a = path[i, 6];
                            rd.WriteItem(joints, i);
                            //Debug.Write(i + "-");
                        }
                        //Debug.Write("\n");
                    }
                }
                success = true;
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool sendPosePath2(float[,] path, int end, int start = 0)
        {
            bool success = false;
            try
            {
                using (this.requestMastership())
                {
                    RapidData rd = synchronizeRapid(task, module, "askPathP");

                    if (rd.Value is ArrayData)
                    {
                        RobTarget goal = default(RobTarget);
                        for (int i = start; i < end; i++)
                        {
                            goal.Trans.X = path[i, 0];
                            goal.Trans.Y = path[i, 1];
                            goal.Trans.Z = path[i, 2];
                            goal.Rot.Q1 = path[i, 3];
                            goal.Rot.Q2 = path[i, 4];
                            goal.Rot.Q3 = path[i, 5];
                            goal.Rot.Q4 = path[i, 6];
                            goal.Extax.Eax_a = path[i, 7];
                            rd.WriteItem(goal, i);
                        }
                        //Debug.Write("\n");
                    }
                }
                success = true;
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
            }

            return success;
        }

        public bool sendJointPathOrder2(float[,] path, float dt)
        {
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            bool write = sendTimestep(dt);
            if (!write)
                return false;

            Num val = default(Num);
            val.Value = path.GetLength(0);
            write = writeData(task, module, "finalJSize", val);
            if (!write)
                return false;

            int step = WINDOW;
            if (step > path.GetLength(0))
                step = path.GetLength(0);
            write = sendJointPath2(path, step-1);
            if (!write)
                return false;
            val.Value = step;
            write = writeData(task, module, "askPathJSize", val);
            if (!write)
                return false;

            write = sendOrder(JOINT_ORDER2);
            if (!write)
                return false;

            //Debug.WriteLine("Sending order took " + watch.ElapsedMilliseconds + " ms");
            //watch.Restart();

            for(int i = 2; i <= path.GetLength(0) / WINDOW; i++)
            {
                step = i * WINDOW;
                write = sendJointPath2(path, step - 1, (i - 1) * WINDOW);
                if (!write)
                    return false;
                val.Value = i * WINDOW;
                write = writeData(task, module, "askPathJSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Sending path step " + i + " took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

            }

            if(path.GetLength(0) > step)
            {
                write = sendJointPath2(path, path.GetLength(0) - 1, step);
                if (!write)
                    return false;
                val.Value = path.GetLength(0);
                write = writeData(task, module, "askPathJSize", val);
                if (!write)
                    return false;
            }

            //Debug.WriteLine("Sending last bit took " + watch.ElapsedMilliseconds + " ms");
            //watch.Stop();

            return true;
        }

        public bool sendJointPathOrder3(float[,] path, float dt)
        {
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            bool write = sendTimestep(dt);
            if (!write)
                return false;

            Num val = default(Num);
            val.Value = path.GetLength(0);
            write = writeData(task, module, "finalJSize", val);
            if (!write)
                return false;

            int step = WINDOW;
            if (step > path.GetLength(0))
                step = path.GetLength(0);
            write = sendJointPath2(path, step - 1);
            if (!write)
                return false;
            val.Value = step;
            write = writeData(task, module, "askPathJSize", val);
            if (!write)
                return false;

            write = sendOrder(JOINT_PATH_ORDER3);
            if (!write)
                return false;

            //Debug.WriteLine("Sending order took " + watch.ElapsedMilliseconds + " ms");
            //watch.Restart();

            for (int i = 2; i <= path.GetLength(0) / WINDOW; i++)
            {
                step = i * WINDOW;
                write = sendJointPath2(path, step - 1, (i - 1) * WINDOW);
                if (!write)
                    return false;
                val.Value = i * WINDOW;
                write = writeData(task, module, "askPathJSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Sending path step " + i + " took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

            }

            if (path.GetLength(0) > step)
            {
                write = sendJointPath2(path, path.GetLength(0) - 1, step);
                if (!write)
                    return false;
                val.Value = path.GetLength(0);
                write = writeData(task, module, "askPathJSize", val);
                if (!write)
                    return false;
            }

            //Debug.WriteLine("Sending last bit took " + watch.ElapsedMilliseconds + " ms");
            //watch.Stop();

            return true;
        }

        public bool sendPosePathOrder2(float[,] path, float dt)
        {
            //Stopwatch watch = new Stopwatch();
            //watch.Start();

            bool write = sendTimestep(dt);
            if (!write)
                return false;

            Num val = default(Num);
            val.Value = path.GetLength(0);
            write = writeData(task, module, "finalPSize", val);
            if (!write)
                return false;

            int step = WINDOW;
            if (step > path.GetLength(0))
                step = path.GetLength(0);
            write = sendPosePath2(path, step - 1);
            if (!write)
                return false;
            val.Value = step;
            write = writeData(task, module, "askPathPSize", val);
            if (!write)
                return false;

            write = sendOrder(JOINT_ORDER2);
            if (!write)
                return false;

            //Debug.WriteLine("Sending order took " + watch.ElapsedMilliseconds + " ms");
            //watch.Restart();

            for (int i = 2; i <= path.GetLength(0) / WINDOW; i++)
            {
                step = i * WINDOW;
                write = sendPosePath2(path, step - 1, (i - 1) * WINDOW);
                if (!write)
                    return false;
                val.Value = i * WINDOW;
                write = writeData(task, module, "askPathPSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Sending path step " + i + " took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

            }

            if (path.GetLength(0) > step)
            {
                write = sendPosePath2(path, path.GetLength(0) - 1, step);
                if (!write)
                    return false;
                val.Value = path.GetLength(0);
                write = writeData(task, module, "askPathPSize", val);
                if (!write)
                    return false;
            }

            //Debug.WriteLine("Sending last bit took " + watch.ElapsedMilliseconds + " ms");
            //watch.Stop();

            return true;
        }

        public bool sendJointPath(float[,] path)
        {
            IRapidData[] rapidPath = new IRapidData[path.GetLength(0)];
            JointTarget joints = default(JointTarget);
            for (int i = 0; i < path.GetLength(0); i++)
            {
                joints.RobAx.Rax_1 = path[i,0];
                joints.RobAx.Rax_2 = path[i,1];
                joints.RobAx.Rax_3 = path[i,2];
                joints.RobAx.Rax_4 = path[i,3];
                joints.RobAx.Rax_5 = path[i,4];
                joints.RobAx.Rax_6 = path[i,5];
                joints.ExtAx.Eax_a = path[i,6];
                rapidPath[i] = joints;
            }

            return this.writeArray(task, module, "askPathJ", rapidPath);
        }

        public bool sendJointPathOrder(float[,] path, float dt)
        {
            bool write = false;
            
            write = sendJointPath(path);

            if (!write)
                return false;

            Num size = default(Num);
            size.Value = path.GetLength(0);
            write = this.writeData(task, module, "askPathJSize", size);

            if (!write)
                return false;

            write = sendTimestep(dt);

            if (!write)
                return false;

            return sendOrder(JOINT_PATH_ORDER);
        }

        public bool sendPosePath(float[,] path)
        {
            IRapidData[] rapidPath = new IRapidData[path.GetLength(0)];
            RobTarget goal = default(RobTarget);
            for (int i = 0; i < path.GetLength(0); i++)
            {
                goal.Trans.X = path[i,0];
                goal.Trans.Y = path[i,1];
                goal.Trans.Z = path[i,2];
                goal.Rot.Q1 = path[i,3];
                goal.Rot.Q2 = path[i,4];
                goal.Rot.Q3 = path[i,5];
                goal.Rot.Q4 = path[i,6];
                goal.Extax.Eax_a = path[i,7];
                rapidPath[i] = goal;
            }

            return writeArray(task, module, "askPathP", rapidPath);
        }

        public bool sendPosePathOrder(float[,] path, float dt)
        {
            bool write = false;

            write = sendPosePath(path);

            if (!write)
                return false;

            Num size = default(Num);
            size.Value = path.GetLength(0);
            write = this.writeData(task, module, "askPathPSize", size);

            if (!write)
                return false;

            write = sendTimestep(dt);

            if (!write)
                return false;

            return sendOrder(POSE_PATH_ORDER);
        }

        public bool sendJoints(float[] pose)
        {
            JointTarget joints = default(JointTarget);

            joints.RobAx.Rax_1 = pose[0];
            joints.RobAx.Rax_2 = pose[1];
            joints.RobAx.Rax_3 = pose[2];
            joints.RobAx.Rax_4 = pose[3];
            joints.RobAx.Rax_5 = pose[4];
            joints.RobAx.Rax_6 = pose[5];
            joints.ExtAx.Eax_a = pose[6];

            return writeData(task, module, "askGoalJ", joints);
        }

        public bool sendJointOrder(float[] pose, float duration)
        {
            bool write = false;
            write = sendJoints(pose);

            if (!write)
                return false;

            write = sendDuration(duration);

            if (!write)
                return false;

            return sendOrder(JOINT_ORDER);
        }

        public bool sendGoToOrder(float[] pose, float duration)
        {
            bool write = false;
            write = sendJoints(pose);

            if (!write)
                return false;

            write = sendDuration(duration);

            if (!write)
                return false;

            return sendOrder(GOTO_ORDER);
        }

        public bool sendPose(float[] pose)
        {
            RobTarget goal = default(RobTarget);

            goal.Trans.X = pose[0];
            goal.Trans.Y = pose[1];
            goal.Trans.Z = pose[2];
            goal.Rot.Q1 = pose[3];
            goal.Rot.Q2 = pose[4];
            goal.Rot.Q3 = pose[5];
            goal.Rot.Q4 = pose[6];
            goal.Extax.Eax_a = pose[7];

            return writeData(task, module, "askGoalP", goal);
        }

        public bool sendPoseOrder(float[] pose, float duration)
        {
            bool write = false;
            write = sendPose(pose);

            if (!write)
                return false;

            write = sendDuration(duration);

            if (!write)
                return false;

            return sendOrder(POSE_ORDER);
        }

        public bool sendGoToPoseOrder(float[] pose, float duration)
        {
            bool write = false;
            write = sendPose(pose);

            if (!write)
                return false;

            write = sendDuration(duration);

            if (!write)
                return false;

            return sendOrder(GOTO_POSE_ORDER);
        }

        public bool readPose(ref float[] pose)
        {
            RobTarget handPose = default(RobTarget);
            bool read = readData<RobTarget>(task, module, "curEndPose", ref handPose);
            pose = new float[8];
            if (read && pose.Length == 8)
            {
                pose[0] = handPose.Trans.X;
                pose[1] = handPose.Trans.Y;
                pose[2] = handPose.Trans.Z;
                pose[3] = (float)handPose.Rot.Q1;
                pose[4] = (float)handPose.Rot.Q2;
                pose[5] = (float)handPose.Rot.Q3;
                pose[6] = (float)handPose.Rot.Q4;
                pose[7] = handPose.Extax.Eax_a;
                return true;
            }
            return false;
        }

        public bool readJoints(ref float[] pose)
        {
            JointTarget jointAngles = default(JointTarget);
            bool read = readData<JointTarget>(task, module, "curJointPose", ref jointAngles);
            pose = new float[7];
            if (read)
            {
                pose[0] = jointAngles.RobAx.Rax_1;
                pose[1] = jointAngles.RobAx.Rax_2;
                pose[2] = jointAngles.RobAx.Rax_3;
                pose[3] = jointAngles.RobAx.Rax_4;
                pose[4] = jointAngles.RobAx.Rax_5;
                pose[5] = jointAngles.RobAx.Rax_6;
                pose[6] = jointAngles.ExtAx.Eax_a;
                return true;
            }
            return false;
        }

        public bool readStatus(ref int status)
        {
            Num val = default(Num);
            bool read = readData<Num>(task, module, "execution_status", ref val);
            if (read)
                status = (int)val.Value;
            return read;
        }

        public bool readNBCall(ref int nb_call)
        {
            Num val = default(Num);
            bool read = readData<Num>(task, module, "nb_call", ref val);
            if (read)
                nb_call = (int)val.Value;
            return read;
        }

        public bool readNBEnd(ref int nb_call)
        {
            Num val = default(Num);
            bool read = readData<Num>(task, module, "nb_end", ref val);
            if (read)
                nb_call = (int)val.Value;
            return read;
        }

        public bool readRecordStep(ref float dt)
        {
            Num val = default(Num);
            bool read = readData<Num>(task, module, "recPathStep", ref val);
            if (read)
                dt = (float)val.Value;
            return read;
        }

        public bool readRecord(ref float[,] path)
        {
            Num val = default(Num);
            bool read = readData<Num>(task, module, "recPathJSize", ref val);

            if (!read || (int) val.Value < 2)
                return false;

            path = new float[((int)val.Value) - 1, 7];
            JointTarget joints = default(JointTarget);
            for(int i = 0; i < path.GetLength(0); i++)
            {
                if (!readArrayData<JointTarget>(task, module, "recPathJ", ref joints, i))
                    return false;
                path[i, 0] = joints.RobAx.Rax_1;
                path[i, 1] = joints.RobAx.Rax_2;
                path[i, 2] = joints.RobAx.Rax_3;
                path[i, 3] = joints.RobAx.Rax_4;
                path[i, 4] = joints.RobAx.Rax_5;
                path[i, 5] = joints.RobAx.Rax_6;
                path[i, 6] = joints.ExtAx.Eax_a;
            }
            return true;
        }

        public bool cancelOrder()
        {
            return triggerInterrupt("custom_DO_3");
        }

        public bool softStop(bool value)
        {
            bool val = true;
            if (readSoftStop(ref val))
                if (val != value)
                    return triggerInterrupt("custom_DO_2");
                else
                    return true;
            return false;
        }

        public bool readSoftStop(ref bool value)
        {
            Bool val = default(Bool);
            bool read = readData<Bool>(task, module, "motion_en", ref val);
            if (read)
                value = (bool)val.Value;
            return read;
        }

        public bool readRunning(ref bool value) //TO DO use the shorter version
        {
            Bool val = default(Bool);
            //return (this.controller.Rapid.GetTask(task).ExecutionStatus == TaskExecutionStatus.Running);

            bool read = readData<Bool>(task, module, "running", ref val);
            if (read)
                value = (bool)val.Value;
            return read;
        }

        public bool suspendToggle(bool value)
        {
            bool val = true;
            bool read = readRunning(ref val);
            if(read)
            {
                if (val != value)
                {
                    if (val)
                        return suspend();
                    else
                        return unsuspend();
                }
                return true;
            }
            return false;
        }

        private bool suspend() //TO DO use the shorter version
        {
            //this.controller.Rapid.GetTask(task).Stop(); // need some mastership
            return triggerInterrupt("custom_DO_1");
        }

        private bool unsuspend() //TO DO use the shorter version
        {
            //this.controller.Rapid.GetTask(task).Start() // need some mastership
            Bool val = default(Bool);
            val.Value = true;
            return writeData(task, module, "running", val);
        }

        public bool setCollisionAvoidance(bool value)
        {
            return writeSignalDO("Collision_Avoidance", value);
        }

        public bool getCollisionAvoidance(ref bool value)
        {
            return readSignalDO("Collision_Avoidance", ref value);
        }

        /*** Test functions ***/

        /*
        public static void testAction()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            float[] goal1_f = new float[7];
            float[] goal2_f = new float[7];

            goal1_f[0] = 110.955F;
            goal1_f[1] = -55.6241F;
            goal1_f[2] = 57.1142F;
            goal1_f[3] = 100.74F;
            goal1_f[4] = 43.1731F;
            goal1_f[5] = 98.4289F;
            goal1_f[6] = -144.962F;

            goal2_f[0] = 63.652F;
            goal2_f[1] = -29.9943F;
            goal2_f[2] = 3.89831F;
            goal2_f[3] = 129.254F;
            goal2_f[4] = 60.6849F;
            goal2_f[5] = 168.939F;
            goal2_f[6] = -95.3751F;

            Num order = default(Num);
            order.Value = 1;

            IRapidData[] path = new IRapidData[SIZE_MOTION];

            JointTarget joints = default(JointTarget);
            int size = path.GetLength(0);
            Console.WriteLine(size);
            for (int i = 0;i < size-1;i++)
            {
                joints.RobAx.Rax_1 = goal1_f[0] + (goal2_f[0] - goal1_f[0]) * ((float) i) / ((float) size-1);
                joints.RobAx.Rax_2 = goal1_f[1] + (goal2_f[1] - goal1_f[1]) * ((float)i) / ((float)size - 1);
                joints.RobAx.Rax_3 = goal1_f[2] + (goal2_f[2] - goal1_f[2]) * ((float)i) / ((float)size - 1);
                joints.RobAx.Rax_4 = goal1_f[3] + (goal2_f[3] - goal1_f[3]) * ((float)i) / ((float)size - 1);
                joints.RobAx.Rax_5 = goal1_f[4] + (goal2_f[4] - goal1_f[4]) * ((float)i) / ((float)size - 1);
                joints.RobAx.Rax_6 = goal1_f[5] + (goal2_f[5] - goal1_f[5]) * ((float)i) / ((float)size - 1);
                joints.ExtAx.Eax_a = goal1_f[6] + (goal2_f[6] - goal1_f[6]) * ((float)i) / ((float)size - 1);
                path[i] = joints;
            }

            joints.RobAx.Rax_1 = goal2_f[0];
            joints.RobAx.Rax_2 = goal2_f[1];
            joints.RobAx.Rax_3 = goal2_f[2];
            joints.RobAx.Rax_4 = goal2_f[3];
            joints.RobAx.Rax_5 = goal2_f[4];
            joints.RobAx.Rax_6 = goal2_f[5];
            joints.ExtAx.Eax_a = goal2_f[6];
            path[size - 1] = joints;

            Console.ReadKey();

            try
            {

                bool write = false;
                using (program.requestMastership())
                    write = program.writeArray("T_ROB_R", "MainModule", "askPathJ", path);
                Console.WriteLine(write);

                Console.ReadKey();

                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "state", order);
                Console.WriteLine(write);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }

        public static void testJointMotion()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            float[] goal1_f = new float[7];
            float[] goal2_f = new float[7];

            goal1_f[0] = 110.955F;
            goal1_f[1] = -55.6241F;
            goal1_f[2] = 57.1142F;
            goal1_f[3] = 100.74F;
            goal1_f[4] = 43.1731F;
            goal1_f[5] = 98.4289F;
            goal1_f[6] = -144.962F;

            goal2_f[0] = 63.652F;
            goal2_f[1] = -29.9943F;
            goal2_f[2] = 3.89831F;
            goal2_f[3] = 129.254F;
            goal2_f[4] = 60.6849F;
            goal2_f[5] = 168.939F;
            goal2_f[6] = -95.3751F;
            
            JointTarget goal1 = default(JointTarget);
            goal1.RobAx.Rax_1 = goal1_f[0];
            goal1.RobAx.Rax_2 = goal1_f[1];
            goal1.RobAx.Rax_3 = goal1_f[2];
            goal1.RobAx.Rax_4 = goal1_f[3];
            goal1.RobAx.Rax_5 = goal1_f[4];
            goal1.RobAx.Rax_6 = goal1_f[5];
            goal1.ExtAx.Eax_a = goal1_f[6];

            JointTarget goal2 = default(JointTarget);
            goal2.RobAx.Rax_1 = goal2_f[0];
            goal2.RobAx.Rax_2 = goal2_f[1];
            goal2.RobAx.Rax_3 = goal2_f[2];
            goal2.RobAx.Rax_4 = goal2_f[3];
            goal2.RobAx.Rax_5 = goal2_f[4];
            goal2.RobAx.Rax_6 = goal2_f[5];
            goal2.ExtAx.Eax_a = goal2_f[6];

            Num order = default(Num);
            order.Value = 3;

            try
            {
                bool write = false;
                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "askGoalJ", goal1);
                Console.WriteLine(write);

                Console.ReadKey();

                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "state", order);
                Console.WriteLine(write);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }

        public static void testMotions()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            //[[403.34,-362.191,250],[0,0,1,0],[-2,1,-2,0],[162.985407042,9E+09,9E+09,9E+09,9E+09,9E+09]] in RAPID
            RobTarget goal1 = default(RobTarget);
            goal1.Trans.X = 403.34F;
            goal1.Trans.Y = -362.191F;
            goal1.Trans.Z = 250F;
            goal1.Rot.Q1 = 0;
            goal1.Rot.Q2 = 0;
            goal1.Rot.Q3 = 1;
            goal1.Rot.Q4 = 0;
            goal1.Robconf.Cf1 = -2;
            goal1.Robconf.Cf4 = 1;
            goal1.Robconf.Cf6 = -2;
            goal1.Robconf.Cfx = 0;
            goal1.Extax.Eax_a = 162.985407042F;

            //[[667.310643984,63.965643541,200],[0,-0.382683433,0.923879532,0],[1,1,-2,4],[132.985397887,9E+09,9E+09,9E+09,9E+09,9E+09]]
            RobTarget goal2 = default(RobTarget);
            goal2.Trans.X = 667.310643984F;
            goal2.Trans.Y = 63.965643541F;
            goal2.Trans.Z = 200F;
            goal2.Rot.Q1 = 0;
            goal2.Rot.Q2 = -0.382683433;
            goal2.Rot.Q3 = 0.923879532;
            goal2.Rot.Q4 = 0;
            goal2.Robconf.Cf1 = 1;
            goal2.Robconf.Cf4 = 1;
            goal2.Robconf.Cf6 = -2;
            goal2.Robconf.Cfx = 4;
            goal2.Extax.Eax_a = 132.985397887F;

            Num order = default(Num);
            order.Value = 2;

            try
            {
                bool write = false;
                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "askGoalP", goal1);
                Console.WriteLine(write);

                Console.ReadKey();

                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "state", order);
                Console.WriteLine(write);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }

        public static void testInterrupts()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            Signal signal1 = program.controller.IOSystem.GetSignal("custom_DI_1");
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                //Signal must be set as accessible (access to ALL)
                diSig1.Set();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();

        }

        public static void testMotionInterrupt()
        {
            YumiLowLevelController program = new YumiLowLevelController("T_ROB_R", "MainModule");

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            //[[403.34,-362.191,250],[0,0,1,0],[-2,1,-2,0],[162.985407042,9E+09,9E+09,9E+09,9E+09,9E+09]] in RAPID
            RobTarget goal1 = default(RobTarget);
            goal1.Trans.X = 403.34F;
            goal1.Trans.Y = -362.191F;
            goal1.Trans.Z = 250F;
            goal1.Rot.Q1 = 0;
            goal1.Rot.Q2 = 0;
            goal1.Rot.Q3 = 1;
            goal1.Rot.Q4 = 0;
            goal1.Robconf.Cf1 = -2;
            goal1.Robconf.Cf4 = 1;
            goal1.Robconf.Cf6 = -2;
            goal1.Robconf.Cfx = 0;
            goal1.Extax.Eax_a = 162.985407042F;

            //[[667.310643984,63.965643541,200],[0,-0.382683433,0.923879532,0],[1,1,-2,4],[132.985397887,9E+09,9E+09,9E+09,9E+09,9E+09]]
            RobTarget goal2 = default(RobTarget);
            goal2.Trans.X = 667.310643984F;
            goal2.Trans.Y = 63.965643541F;
            goal2.Trans.Z = 200F;
            goal2.Rot.Q1 = 0;
            goal2.Rot.Q2 = -0.382683433;
            goal2.Rot.Q3 = 0.923879532;
            goal2.Rot.Q4 = 0;
            goal2.Robconf.Cf1 = 1;
            goal2.Robconf.Cf4 = 1;
            goal2.Robconf.Cf6 = -2;
            goal2.Robconf.Cfx = 4;
            goal2.Extax.Eax_a = 132.985397887F;

            Num order = default(Num);
            order.Value = 2;

            try
            {
                bool write = false;
                write = program.writeData("T_ROB_R", "MainModule", "askGoalP", goal2);
                Console.WriteLine(write);

                Console.ReadKey();

                write = program.writeData("T_ROB_R", "MainModule", "askOrder", order);
                Console.WriteLine(write);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();

            program.readStatus(ref program.status);
            
            //program.triggerInterrupt("custom_DI_1");
        }

        public static void testMotionsV2()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            //[[403.34,-362.191,250],[0,0,1,0],[-2,1,-2,0],[162.985407042,9E+09,9E+09,9E+09,9E+09,9E+09]] in RAPID
            float[] goal1 = new float[8];
            goal1[0] = 403.34F;
            goal1[1] = -362.191F;
            goal1[2] = 250F;
            goal1[3] = 0;
            goal1[4] = 0;
            goal1[5] = 1;
            goal1[6] = 0;
            goal1[7] = -135F;

            //[[667.310643984,63.965643541,200],[0,-0.382683433,0.923879532,0],[1,1,-2,4],[132.985397887,9E+09,9E+09,9E+09,9E+09,9E+09]]
            float[] goal2 = new float[8];
            //goal2[0] = 667.310643984F;
            //goal2[1] = 63.965643541F;
            //goal2[2] = 200F;
            //goal2[3] = 0;
            //goal2[4] = -0.382683433F;
            //goal2[5] = 0.923879532F;
            //goal2[6] = 0;
            //goal2[7] = -135F;
            goal1[0] = 403.34F;
            goal1[1] = -362.191F;
            goal1[2] = 550F;
            goal1[3] = 0;
            goal1[4] = 0;
            goal1[5] = 1;
            goal1[6] = 0;
            goal1[7] = -135F;

            Num order = default(Num);
            order.Value = 2;

            bool write = program.sendPoseOrder(goal1, 3.0F);
            Console.WriteLine(write);
            Console.ReadKey();
        }

        public static void testReadWrite()
        {
            YumiLowLevelController program = new YumiLowLevelController();

            if (program.connectRobot())
            {
                Console.WriteLine("Congratulations! You\'re online.");
            }
            else
            {
                Console.WriteLine("Selected controller not available.");
                return;
            }

            Console.ReadKey();

            try
            {
                Num duration = default(Num);
                bool read = program.readData("T_ROB_R", "MainModule", "curDuration", ref duration);
                Console.WriteLine(read);
                Console.WriteLine("curDuration = " + duration);

                duration.Value = 4;
                bool write = false;
                using (program.requestMastership())
                    write = program.writeData("T_ROB_R", "MainModule", "curDuration", duration);
                Console.WriteLine(write);

                Num duration2 = default(Num);
                read = program.readData("T_ROB_R", "MainModule", "curDuration", ref duration2);
                Console.WriteLine(read);
                Console.WriteLine("curDuration = " + duration2);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }
        */

        static void Main(string[] args)
        {
            //CommunicationServer srv = new CommunicationServer("@tcp://*:5556");
            //srv.main();

            //testMotionsV2();
            //testCurrentPose();
            //testJointMotion();
            //testMotions();
            //testInterrupts();
            //testMotionInterrupt();

            NetworkScanner scanner = new NetworkScanner();
            scanner.Scan();
            ControllerInfoCollection controllers = scanner.Controllers;

            Console.WriteLine("Scanning for controllers!");

            ControllerInfo controllerInfo = controllers[0];

            YumiLowLevelController controller = new YumiLowLevelController("T_ROB_R", "TestModule2");
            controller.connectRobot(controllerInfo);

            Console.WriteLine("Controller online!");

            bool run = controller.isRunning();
            Console.WriteLine("Task running? " + run);

            Console.ReadKey();

            if (run)
            {
                controller.stopTask();
            }
            else
            {
                controller.setPointer("increment", 1);
                controller.startTask();
            }

            run = controller.isRunning();
            Console.WriteLine("Task running? " + run);

            Console.ReadKey();

            if (run)
            {
                controller.stopTask();
            }

            run = controller.isRunning();
            Console.WriteLine("Task running? " + run);
            Console.ReadKey();

            controller.setPointer("decrement", 1);
            controller.startTask();

            run = controller.isRunning();
            Console.WriteLine("Task running? " + run);
            Console.ReadKey();

        }
    }
}
