﻿using System;
using System.Collections.Generic;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Diagnostics;
using System.Dynamic;

namespace YumiControlPC
{
    public class YumiListener
    {
        // Attributes used to launch and manage server
        public bool working = true;
        private int step;
        private int frame = 0;
        private string srv;
        private PublisherSocket pub = null;
        private YumiLowLevelController controller;

        // Attributes corresponding to actual RAPID values
        private bool running = false;
        private bool motion_en = false;
        private int status = 0;
        private int nb_call = 0;
        private int nb_end = 0;
        //private float[] curEndPose = new float[8];
        //private float[] curJoints = new float[7];
        private bool collision_en = false;

        // Attribute showing which readings failed
        private bool[] success = new bool[6];

        public YumiListener(YumiLowLevelController controller, string srv, int step = 50)
        {
            this.controller = controller;
            this.srv = srv;
            this.step = step;
        }

        public void refresh()
        {
            success[0] = controller.readRunning(ref running);
            success[1] = controller.readSoftStop(ref motion_en);
            success[2] = controller.readStatus(ref status);
            success[3] = controller.getCollisionAvoidance(ref collision_en);
            success[4] = controller.readNBCall(ref nb_call);
            success[5] = controller.readNBEnd(ref nb_end);
        }

        public void sendUpdate()
        {
            dynamic msg = new ExpandoObject();
            msg.payload = running;
            msg.uptodate = success[0];
            pub.SendMoreFrame("running").SendFrame(JsonConvert.SerializeObject((ExpandoObject) msg));

            msg = new ExpandoObject();
            msg.payload = motion_en;
            msg.uptodate = success[1];
            pub.SendMoreFrame("motion_en").SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

            msg = new ExpandoObject();
            msg.payload = status;
            msg.uptodate = success[2];
            pub.SendMoreFrame("status").SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

            msg = new ExpandoObject();
            msg.payload = collision_en;
            msg.uptodate = success[3];
            pub.SendMoreFrame("collision_en").SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

            msg = new ExpandoObject();
            msg.payload = nb_call;
            msg.uptodate = success[4];
            pub.SendMoreFrame("nb_call").SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

            msg = new ExpandoObject();
            msg.payload = nb_end;
            msg.uptodate = success[5];
            pub.SendMoreFrame("nb_end").SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));

            /*
            msg = new Dictionary<string, string>();
            msg.Add("payload", JsonConvert.SerializeObject(curEndPose));
            msg.Add("uptodate", JsonConvert.SerializeObject(success[3]));
            pub.SendMoreFrame("curEndPose").SendFrame(JsonConvert.SerializeObject(msg));

            msg = new Dictionary<string, string>();
            msg.Add("payload", JsonConvert.SerializeObject(curJoints));
            msg.Add("uptodate", JsonConvert.SerializeObject(success[4]));
            pub.SendMoreFrame("curJoints").SendFrame(JsonConvert.SerializeObject(msg));
            */

        }

        public void run()
        {
            try
            {
                pub = new PublisherSocket();
                pub.Bind(srv);
                Debug.WriteLine("Listener online!");

                while (working)
                {
                    refresh();
                    sendUpdate();
                    Thread.Sleep(step);
                }
                pub.Dispose();
                Debug.WriteLine("Listener offline!");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }
    }
}
