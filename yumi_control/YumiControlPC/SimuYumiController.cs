﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using ABB.Robotics.Controllers;
using ABB.Robotics.Controllers.Discovery;
using ABB.Robotics.Controllers.RapidDomain;
using ABB.Robotics.Controllers.IOSystemDomain;
using System.Threading;
using ABB.Robotics;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Collections;

namespace YumiControlPC
{
    public class SimuYumiController
    {
        private static int BATCH_SIZE = 50;
        private static int defaultSize = 1000;

        private Controller controller = null;  //Controller of the robot
        private Mastership master = null;  //Currently held mastership (mostly used for accessing RAPID domain)
        private string task;
        private string module;
        private Stack<float[]> pathSave = new Stack<float[]>();
        private int primitiveLonger = 0;
        //private float[,] initPosition = { { -25f, 41f, -123f, -70f, -8f, -46f, -88f } };
        private float[,] initPosition = { { -134.36f, 21.66f, -8.05f, 165.91f, 32.73f, -41.27f, 131.37f } };
        private bool IsVirtual = false;
        private string virtualTask = "T_ROB_R_b";
        private string virtualPath = "path.data";
        private string collisionStr = "custom_DO_3";

        public SimuYumiController(string task, string module)
        {
            this.task = task;
            this.module = module;
        }

        /**
         * Low-level functions
         **/
        public void virtualRobotSetup()
        {
            if (IsVirtual) {
                virtualTask = "T_ROB_R_b";
                virtualPath = "path1.data";
            }
            else
            {
                virtualTask = "T_ROB_R";
                virtualPath = "path.data";
            }
        }
        // Connect the controller to the robot
        public bool connectRobot(ControllerInfo controllerInfo, string user = null, string pwd = null)
        {
            //scanner = new NetworkScanner();
            //scanner.Scan();
            //ControllerInfoCollection controllers = scanner.Controllers;

            //ControllerInfo controllerInfo = controllers[0];

            if
                (controllerInfo.Availability == ABB.Robotics.Controllers.Availability.Available)
            {
                if (controller != null)
                {
                    return false;
                }
                controller = ControllerFactory.CreateFrom(controllerInfo);
                if (user == null || pwd == null)
                    controller.Logon(UserInfo.DefaultUser);
                else
                {
                    UserInfo info = new UserInfo(user, pwd);
                    controller.Logon(info);
                }
                IsVirtual = controllerInfo.IsVirtual;
                virtualRobotSetup();
                return (controller.OperatingMode == ControllerOperatingMode.Auto);
            }
            return false;
        }
        
        // Disconnect the controller from the robot
        public bool disconnectRobot()
        {
            if (controller != null)
            {
                controller.Logoff();
                controller.Dispose();
                controller = null;
            }
            return true;
        }

        // Acquire mastership to RAPID domain
        public void requestMastership()
        {
            releaseMastership();
            master = Mastership.Request(controller.Rapid);
        }

        // Release mastership
        public void releaseMastership()
        {
            if (master != null)
                master.Dispose();
            master = null;
        }

        // Return synchronized remote RAPID variable
        private RapidData synchronizeRapid(string task, string module, string varName)
        {
            return controller.Rapid.GetRapidData(task, module, varName);
        }

        // Start the main task
        public bool startTask()
        {
            //Debug.WriteLine("Starting task");
            try
            {
                Task wTask = controller.Rapid.GetTask(task);
                //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
                requestMastership();
                try
                {
                    StartResult res = controller.Rapid.Start(RegainMode.Continue, ExecutionMode.Continuous, ExecutionCycle.AsIs, StartCheck.None, false, TaskPanelExecutionMode.AllTasks);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
                //Debug.WriteLine("Task status: " + wTask.ExecutionStatus);
                return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }
        }

        // Stop the main task
        public bool stopTask()
        {
            //Debug.WriteLine("Stopping task");
            try
            {
                Task wTask = controller.Rapid.GetTask(task);
                //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
                requestMastership();
                try
                {
                    controller.Rapid.Stop(StopMode.Immediate, TaskPanelExecutionMode.AllTasks);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
                //Debug.WriteLine("Task status: " + wTask.ExecutionStatus);
                return (wTask.ExecutionStatus != TaskExecutionStatus.Running);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return false;
            }

        }

        // Return true if the task is running
        public bool isRunning(string task = null)
        {
            //Debug.WriteLine("Getting task");
            Task wTask;
            if (task == null)
                wTask = controller.Rapid.GetTask(this.task);
            else
                wTask = controller.Rapid.GetTask(task);
            //Debug.WriteLine("Got task: " + wTask.ExecutionStatus);
            return (wTask.ExecutionStatus == TaskExecutionStatus.Running);
        }

        // Reset the program pointer of the task
        public bool resetPointer(string thatTask = null)
        {
            Task wTask;
            if (thatTask == null)
                wTask = controller.Rapid.GetTask(task);
            else
                wTask = controller.Rapid.GetTask(thatTask);

            try
            {
                requestMastership();
                try
                {
                    //Debug.WriteLine("Resetting pointer");
                    wTask.ResetProgramPointer();
                    return true;
                }
                catch (GenericControllerException)
                {
                    return true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
                finally
                {
                    releaseMastership();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        // Write on a digital output signal
        public bool writeSignalDO(string id, bool value)
        {
            bool write = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                //Signal must be set as accessible (access to ALL)
                if (value)
                    diSig1.Set();
                else
                    diSig1.Reset();
                write = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }

            return write;
        }

        // Read a digital output signal
        public bool readSignalDO(string id, ref bool value)
        {
            bool read = false;
            Signal signal1 = controller.IOSystem.GetSignal(id);
            DigitalSignal diSig1 = (DigitalSignal)signal1;

            try
            {
                value = diSig1.Get() != 0;
                read = true;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
            }
            return read;
        }

        // Trigger an interrupt by toggling a digital output
        public bool triggerInterrupt(string id)
        {
            bool success = false;
            bool value = true;
            if (!readSignalDO(id, ref value))
                return false;
            if (value)
            {
                success = writeSignalDO(id, false);
                if (!success)
                    return false;
            }
            success = writeSignalDO(id, true);
            if (!success)
                return false;
            success = writeSignalDO(id, false);
            return success;
        }

        // Set the program of the main task
        public bool setPointer(string routine, int line)
        {
            Task wTask = controller.Rapid.GetTask(task);
            try
            {
                requestMastership();
                ProgramPosition pos = new ProgramPosition(module, routine, new TextRange(line));
                wTask.SetProgramPointer(pos);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }
            return false;
        }

        // Read a remote RAPID T typed data and store it in var ref
        public bool readData<T>(string task, string module, string varName, ref T var)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is T)
            {
                var = (T)rd.Value;
                return true;
            }
            return false;
        }

        // Write a RAPID data on the robot
        public bool writeData(string task, string module, string varName, IRapidData var)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);
                if (rd.Value.GetType().IsAssignableFrom(var.GetType()))
                {
                    rd.Value = var;
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Write a RAPID data on the robot in a RAPID array at position i
        public bool writeArrayData(string task, string module, string varName, IRapidData var, int i)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);
                if (rd.Value is ArrayData)
                {
                    rd.WriteItem(var, i);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Read a remote RAPID data from an array at position i and store it in var ref
        public bool readArrayData<T>(string task, string module, string varName, ref T var, int i)
        {
            RapidData rd = synchronizeRapid(task, module, varName);
            if (rd.Value is ArrayData)
            {
                var = (T)rd.ReadItem(i);
                return true;
            }
            return false;
        }


        /**
         * Sligthly more complex functions
         * */

        // Move robot along given trajectory and store final 
        public bool moveRobotEasy(float[,] path, float timestep, ref float[] finalPose, ref float[] joints, ref int exeTime, bool isreset)
        {
            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status entering: " + intStatus);

                Num dt = default(Num);
                dt.Value = timestep;

                bool write = writeData(task, module, "timestep", dt);

                Bool yay = default(Bool);
                yay.Value = false;
                writeData(task, module, "isReset", yay);
                if (!write)
                    return false;

                //Debug.WriteLine("Timestep sent");              

                //string pathFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\path.data";
                string pathFile = virtualPath;
                if (File.Exists(pathFile))
                    File.Delete(pathFile);                
                serializePathAndSave(pathFile, path, isreset);
                controller.FileSystem.PutFile(pathFile, "path.data");

                //Debug.WriteLine("Current size sent");

                if (!startTask())
                {
                    Debug.WriteLine("startTask failed");
                }

                //Debug.WriteLine("Initialize motion took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Thread.Sleep(10000);

                //Debug.WriteLine("Task started: motion begins!");


                //Debug.WriteLine("Full motion sent");
                /*Debug.WriteLine("path.GetLength: " + path.GetLength(0));
                int pathLength = path.GetLength(0);
                Num Val = default(Num);
                do {                    
                    readData<Num>(task, module, "finalSize", ref Val);
                    Debug.WriteLine("In while, finalSize: " + Val.Value);
                    Thread.Sleep(10);
                } while (Val.Value != pathLength);

                //float[,] positon = new float[pathLength, 7];
                //ArrayList positionList = new ArrayList();
                RobTarget position = default(RobTarget);
                float x = 0.0f;
                float y = 0.0f;
                float z = 0.0f;
                float distance = 0.0f;
                float vMax = 1000f;
                float vPrdt = 0;
                Num newTimestep = default(Num);
                readData<RobTarget>(task, module, "prePose", ref position);
                Debug.WriteLine("Pre Position: " + position.ToString());
                Debug.WriteLine("Pre Position trans: " + position.Trans);
                //positionList.Add(position);

                for (int i = 0; i < pathLength; ++i)
                {
                    x = position.Trans.X;
                    y = position.Trans.Y;
                    z = position.Trans.Z;
                    readArrayData<RobTarget>(task, module, "position", ref position, i);
                    Debug.WriteLine("Position: " + position.ToString());
                    distance = (float)Math.Sqrt(Math.Pow((position.Trans.X - x),2) + Math.Pow((position.Trans.Y - y),2) + Math.Pow((position.Trans.Z - z),2));
                    Debug.WriteLine("Distance: " + distance);
                    vPrdt = distance / timestep;
                    if (vPrdt > vMax)
                    {
                        newTimestep.Value = 10;//distance / vMax;
                        Debug.WriteLine("Timestep: " + newTimestep.Value);
                    }
                    else
                    {
                        newTimestep.Value = timestep;
                    }                    
                    writeArrayData(task, module, "timeSteps", newTimestep, i);
                } */               

                while (isRunning())
                    Thread.Sleep(100);
                Bool stopping = default(Bool);
                stopping.Value = true;
                write = writeData(task, module, "stopsync", stopping);
                while (isRunning(virtualTask)) //"T_ROB_R_b"
                    Thread.Sleep(100);

                //Debug.WriteLine("Moving and detecting end took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Motion ended");

                RobTarget endPose = default(RobTarget);
                readData<RobTarget>(task, module, "finalPose", ref endPose);
                finalPose[0] = endPose.Trans.X;
                finalPose[1] = endPose.Trans.Y;
                finalPose[2] = endPose.Trans.Z;
                finalPose[3] = (float)endPose.Rot.Q1;
                finalPose[4] = (float)endPose.Rot.Q2;
                finalPose[5] = (float)endPose.Rot.Q3;
                finalPose[6] = (float)endPose.Rot.Q4;

                JointTarget endJoints = default(JointTarget);
                readData<JointTarget>(task, module, "finalJoints", ref endJoints);
                joints[0] = endJoints.RobAx.Rax_1;
                joints[1] = endJoints.RobAx.Rax_2;
                joints[2] = endJoints.RobAx.Rax_3;
                joints[3] = endJoints.RobAx.Rax_4;
                joints[4] = endJoints.RobAx.Rax_5;
                joints[5] = endJoints.RobAx.Rax_6;
                joints[6] = endJoints.ExtAx.Eax_a;

                //Debug.WriteLine("Getting final state took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                Num time = default(Num);                
                readData<Num>(task, module, "exeTime", ref time);
                exeTime = (int)(time.Value * 1000);
                Debug.WriteLine("exeTime for move is : " + exeTime);

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                if (IsVirtual)
                {
                    Debug.WriteLine("Virtual machine, trying to simulate collision!");
                    bool collision = false;
                    if (!readSignalDO(collisionStr, ref collision))
                    {
                        Debug.WriteLine("Read collision signal failed!");
                    }
                    else if (collision)
                    {
                        Debug.WriteLine("Collision detected!");
                    }
                    return (newStatus > intStatus) && (!collision);
                }
                else
                {
                    return (newStatus > intStatus);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        public bool moveRobotEasy_js(float[,] path, float timestep, ref int exeTime, bool isreset)
        {
            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status entering: " + intStatus);

                Num dt = default(Num);
                dt.Value = timestep;

                bool write = writeData(task, module, "timestep", dt);
                if (!write)
                    return false;

                //Debug.WriteLine("Timestep sent");

                Num val = default(Num);
                readData<Num>(task, module, "finalSize", ref val);

                //string pathFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\path.data";
                string pathFile = virtualPath;
                if (File.Exists(pathFile))
                    File.Delete(pathFile);
                serializePathAndSave(pathFile, path, isreset);
                controller.FileSystem.PutFile(pathFile, "path.data");

                //Debug.WriteLine("Current size sent");

                if (!startTask())
                {
                    Debug.WriteLine("startTask failed");
                }


                //Debug.WriteLine("Full motion sent");

                while (isRunning())
                    Thread.Sleep(100);

                Bool stopping = default(Bool);
                stopping.Value = true;
                write = writeData(task, module, "stopsync", stopping);

                while (isRunning(virtualTask))
                    Thread.Sleep(100);

                //Debug.WriteLine("Motion ended");
                Num time = default(Num);
                readData<Num>(task, module, "exeTime", ref time);
                exeTime = (int)(time.Value * 1000);
                Debug.WriteLine("exeTime for reset is : " + exeTime);

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;
                //Debug.WriteLine("Finish");
                return (newStatus > intStatus);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }


        // Reset the robot controller before a motion execution
        public bool resetRobot(float timestep, int downSamplingNumerator, int downSamplingDenominator, ref int exeTime)
        {
			bool rtval = false;
			if (IsVirtual) {
				pathSave.Clear();
				if (isRunning())
					stopTask();
				resetPointer();
				resetPointer(virtualTask);
				Bool yay = default(Bool);
				yay.Value = false;
				writeData(task, module, "startsync", yay);
				yay.Value = true;
				writeData(task, module, "stopsync", yay);
				// reset collision signal
				writeSignalDO(collisionStr, false); 
				rtval = startTask();
			}            
			else  // reset the real robot
			{
				rtval = resetRobot_js(timestep, downSamplingNumerator, downSamplingDenominator, ref exeTime);
			}
            return rtval;
        }

        public bool resetRobot_js(float timestep, int downSamplingNumerator, int downSamplingDenominator, ref int exeTime)
		{
            Num lastIndex = default(Num);
            readData<Num>(task, module, "global_index", ref lastIndex);
            if (isRunning())
				stopTask();
            resetPointer();
            Bool yay = default(Bool);
            yay.Value = false;
            writeData(task, module, "startsync", yay);
            yay.Value = true;
            writeData(task, module, "stopsync", yay);
            if (IsVirtual)
            {
                writeSignalDO(collisionStr, false);
            }

            if (!startTask())
            {
                Debug.WriteLine("Reset_js startTask failed");
                return false;
            }
			//discard the tail positions
			
			bool resetFlag = false;


			int i = 0;
			int index = ((int)(lastIndex.Value) - 1 > 0) ? (int)(lastIndex.Value) - 1 : 0;
			Debug.WriteLine("primitiveLonger : " + primitiveLonger);
			Debug.WriteLine("index : " + index);
			for (i = 0; i < primitiveLonger - index + 1; ++i)
			{
				if (pathSave.Count > 0)
				{
					pathSave.Pop();
				}
			}

			//transform stack to table;			

			//int downSamplingNumerator = 2;
			//int downSamplingDenominator = 3;
			int recoverLength = pathSave.Count;
			Debug.WriteLine("reset traject total length: " + recoverLength);
			if (recoverLength != 0) // need to recover
			{
				if (downSamplingNumerator > downSamplingDenominator)
				{
					Debug.WriteLine("downSampling Factor should be smaller than 1 !");
				}

				int pathLength = ((int)Math.Floor((float)(recoverLength / downSamplingDenominator))) * downSamplingNumerator;
				pathLength += (recoverLength % downSamplingDenominator > downSamplingNumerator) ? downSamplingNumerator : recoverLength % downSamplingDenominator;
				float[,] path = new float[pathLength, 7];

				int count = 0;
				i = 0;
				// down sampling
				foreach (var itm in pathSave)
				{
					// only copy path[] on the sampling point
					if (count < downSamplingNumerator)
					{
						for (int j = 0; j < 7; j++)
						{
							path[i, j] = itm[j];
						}
						//Debug.WriteLine("i: " + i + " count: " + count + " Path is: " + path[i, 0]);

						++i;
					}
					count = (count == (downSamplingDenominator - 1)) ? 0 : ++count;
				}
				Debug.WriteLine("reset traject length after downsampling: " + i);

				//wait utill the init procedure in RAPID is finished
				while (isRunning())
					Thread.Sleep(50);

				int completeChunkNum = (int)Math.Floor((float)(pathLength / defaultSize));
				int restLength = pathLength - completeChunkNum * defaultSize;
				float[,] pathChunk = new float[defaultSize, 7];
				float[,] pathChunkRest = new float[restLength + 1, 7]; // Add init position to table tail to ensure the end posture

				int startPos = 0;
				//distribute the total path into chunks
				for (i = 0; i < completeChunkNum; ++i)
				{
					startPos = i * defaultSize * 7; // The array is 2D, need to mul 7
					Array.Copy(path, startPos, pathChunk, 0, defaultSize * 7);
					//roll back the robot                
					if (!(resetFlag = moveRobotEasy_js(pathChunk, timestep, ref exeTime, true)))
					{
						Debug.WriteLine("Reset Fail: Chunk No." + i);
						pathSave.Clear();
						return resetFlag;
					}
					else
					{
						Debug.WriteLine("Reset Chunk No." + i + " Executed !");
					}
				}
				Array.Copy(path, defaultSize * completeChunkNum * 7, pathChunkRest, 0, restLength * 7);
				Array.Copy(initPosition, 0, pathChunkRest, restLength * 7, 7);

				if (!(resetFlag = moveRobotEasy_js(pathChunkRest, timestep, ref exeTime, true)))
				{
					Debug.WriteLine("Reset Fail: Last Chunk.");
					pathSave.Clear();
					Debug.WriteLine("Try to goto initposition in 3 second.");
					float[] initpose = { initPosition[0, 0], initPosition[0, 1], initPosition[0, 2], initPosition[0, 3], initPosition[0, 4], initPosition[0, 5], initPosition[0, 6] };
					//goToInitPosition(initpose, 3);
					return resetFlag;
				}
				else
				{
					Debug.WriteLine("Reset Last Chunk Executed !");
				}
				/*for (i = 0; i < restLength + 1; ++i)
				{
					Debug.WriteLine("reset: " + pathChunkRest[i , 0]);
				}*/

				pathSave.Clear();
			}
			else {
				resetFlag = true;
			}
            return resetFlag;
        }

        // Serialize and save a trajectory
        public void serializePathAndSave(string fileName, float[,] path, bool isreset)
        {
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(fileName);

                for (int i = 0; i < path.GetLength(0); i++)
                {
                    string line = null;
                    float[] floatLine = new float[path.GetLength(1)];                    
                    for (int j = 0; j < path.GetLength(1); j++)
                    {
                        floatLine[j] = path[i, j];
                    }
                    if (!isreset)
                    {
                        pathSave.Push(floatLine);
                    }                  
                    line = JsonConvert.SerializeObject(floatLine);
                    //Debug.WriteLine("line is:" + line);                    
                    sw.WriteLine(line);
                }
                primitiveLonger = path.GetLength(0);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        // Deserialize and save a trajectory
        public void deserializeTrajAndLoad(string fileName, ref float[,] path)
        {
            string line;
            dynamic dico = null;
            float[] floatLine;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(fileName);

                //Read the first line of text
                line = sr.ReadLine();

                //Continue to read until you reach end of file
                int i = 0;
                while (line != null && i < path.GetLength(0))
                {
                    dico = JsonConvert.DeserializeObject(line);                    
                    floatLine = dico.ToObject<float[]>();
                    for (int j = 0; j < path.GetLength(1); j++)
                    {
                        path[i, j] = floatLine[j];                        
                    }
                    i += 1;
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }


        /**
         *  DEPRECATED AND UNTESTED
         **/

        public bool writeArray(string task, string module, string varName, IRapidData[] var)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);

                if (rd.Value is ArrayData)
                {
                    for (int i = 0; i < var.Length; i++)
                    {
                        rd.WriteItem(var[i], i);
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        public bool writeJoint(string task, string module, string varName, float[] pose)
        {
            bool success = false;
            try
            {
                JointTarget joints = default(JointTarget);
                joints.RobAx.Rax_1 = pose[0];
                joints.RobAx.Rax_2 = pose[1];
                joints.RobAx.Rax_3 = pose[2];
                joints.RobAx.Rax_4 = pose[3];
                joints.RobAx.Rax_5 = pose[4];
                joints.RobAx.Rax_6 = pose[5];
                joints.ExtAx.Eax_a = pose[6];
                joints.ExtAx.Eax_b = 9000000000;
                joints.ExtAx.Eax_c = 9000000000;
                joints.ExtAx.Eax_d = 9000000000;
                joints.ExtAx.Eax_e = 9000000000;
                joints.ExtAx.Eax_f = 9000000000;
                writeData(task, module, varName, joints);
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                releaseMastership();
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        public bool writePath(string task, string module, string varName, float[,] path, int end, int start = 0)
        {
            bool success = false;
            try
            {
                requestMastership();
                RapidData rd = synchronizeRapid(task, module, varName);

                if (rd.Value is ArrayData)
                {
                    JointTarget joints = default(JointTarget);
                    for (int i = start; i <= end; i++)
                    {
                        joints.RobAx.Rax_1 = path[i, 0];
                        joints.RobAx.Rax_2 = path[i, 1];
                        joints.RobAx.Rax_3 = path[i, 2];
                        joints.RobAx.Rax_4 = path[i, 3];
                        joints.RobAx.Rax_5 = path[i, 4];
                        joints.RobAx.Rax_6 = path[i, 5];
                        joints.ExtAx.Eax_a = path[i, 6];
                        rd.WriteItem(joints, i);
                    }
                }
                success = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                releaseMastership();
            }

            return success;
        }

        // Move robot along given trajectory (OLD)
        public bool moveRobot(float[,] path, float timestep, ref float[] finalPose, ref float[] joints, ref float[,] trajectory)
        {
            try
            {
                //Stopwatch watch = new Stopwatch();
                //watch.Start();

                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + intStatus);

                Num dt = default(Num);
                dt.Value = timestep;

                bool write = writeData(task, module, "timestep", dt);
                if (!write)
                    return false;

                //Debug.WriteLine("Timestep sent");

                Num val = default(Num);
                val.Value = path.GetLength(0);
                write = writeData(task, module, "finalSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Final size sent");

                int step = BATCH_SIZE;
                if (step > path.GetLength(0))
                    step = path.GetLength(0);
                write = writePath(task, module, "path", path, step - 1);
                if (!write)
                    return false;

                //Debug.WriteLine("First motion batch sent");

                val.Value = step;
                write = writeData(task, module, "curSize", val);
                if (!write)
                    return false;

                //Debug.WriteLine("Current size sent");

                startTask();

                //Debug.WriteLine("Initialize motion took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Thread.Sleep(10000);

                //Debug.WriteLine("Task started: motion begins!");

                for (int i = 2; i <= path.GetLength(0) / BATCH_SIZE; i++)
                {
                    step = i * BATCH_SIZE;
                    write = writePath(task, module, "path", path, step - 1, (i - 1) * BATCH_SIZE);
                    if (!write)
                        return false;
                    val.Value = i * BATCH_SIZE;
                    write = writeData(task, module, "curSize", val);
                    if (!write)
                        return false;

                }

                if (path.GetLength(0) > step)
                {
                    write = writePath(task, module, "path", path, path.GetLength(0) - 1, step);
                    if (!write)
                        return false;
                    val.Value = path.GetLength(0);
                    write = writeData(task, module, "curSize", val);
                    if (!write)
                        return false;
                }

                //Debug.WriteLine("Full motion sent");

                while (isRunning() || isRunning(virtualTask))
                    Thread.Sleep(100);

                //Debug.WriteLine("Moving and detecting end took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Motion ended");

                RobTarget endPose = default(RobTarget);
                readData<RobTarget>(task, module, "finalPose", ref endPose);
                finalPose[0] = endPose.Trans.X;
                finalPose[1] = endPose.Trans.Y;
                finalPose[2] = endPose.Trans.Z;
                finalPose[3] = (float)endPose.Rot.Q1;
                finalPose[4] = (float)endPose.Rot.Q2;
                finalPose[5] = (float)endPose.Rot.Q3;
                finalPose[6] = (float)endPose.Rot.Q4;

                //Debug.WriteLine("Got final pose");

                JointTarget endJoints = default(JointTarget);
                readData<JointTarget>(task, module, "finalJoints", ref endJoints);
                joints[0] = endJoints.RobAx.Rax_1;
                joints[1] = endJoints.RobAx.Rax_2;
                joints[2] = endJoints.RobAx.Rax_3;
                joints[3] = endJoints.RobAx.Rax_4;
                joints[4] = endJoints.RobAx.Rax_5;
                joints[5] = endJoints.RobAx.Rax_6;
                joints[6] = endJoints.ExtAx.Eax_a;

                //Debug.WriteLine("Getting final state took " + watch.ElapsedMilliseconds + " ms");
                //watch.Restart();

                //Debug.WriteLine("Got final joints");

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + newStatus);

                return (newStatus > intStatus);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }

        // Move robot to final spatial position in given duration (NOT USED)
        public bool goTo(float[] pose, float duration)
        {
            try
            {
                if (isRunning())
                    stopTask();

                //Debug.WriteLine("Ready to send parameters");

                Num status = default(Num);
                readData<Num>(task, module, "exec_status", ref status);
                int intStatus = (int)status.Value;

                //Debug.WriteLine("Execution status: " + intStatus);

                Num dt = default(Num);
                dt.Value = duration;

                bool write = writeData(task, module, "duration", dt);
                if (!write)
                    return false;

                RobTarget endPose = default(RobTarget);
                endPose.Trans.X = pose[0];
                endPose.Trans.Y = pose[1];
                endPose.Trans.Z = pose[2];
                endPose.Rot.Q1 = pose[3];
                endPose.Rot.Q2 = pose[4];
                endPose.Rot.Q3 = pose[5];
                endPose.Rot.Q4 = pose[6];
                write = writeData(task, module, "goal", endPose);

                if (!write)
                    return false;

                startTask();

                while (isRunning())
                    Thread.Sleep(100);

                readData<Num>(task, module, "exec_status", ref status);
                int newStatus = (int)status.Value;

                return (newStatus > intStatus);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return false;
        }
        
        // Move robot to final spatial position in given duration (NOT USED)
        public bool goToInitPosition(float[] pose, float duration)
        {           
            try
            {
                if (isRunning())
                    stopTask();

                // reset global variable initPosition
                for (int i = 0; i < 7; ++i)
                {
                    initPosition[0, i] = pose[i];
                }
                
                Num dt = default(Num);
                dt.Value = duration;
                bool write = writeData(task, module, "timestep", dt);
                if (!write)
                {
                    Debug.WriteLine("writeData timestep False.");
                    return false;
                }
                setPointer("gotoinitposition", 1);

                write = writeJoint(task, module, "initposition", pose);
                if (!write)
                {
                    Debug.WriteLine("writeData initposition False.");
                    return false;
                }
                startTask();
                while (isRunning("T_ROB_R"))
                {
                    Thread.Sleep(10);
                }
                if (isRunning())
                    stopTask();

                resetPointer();
                resetPointer(virtualTask);
                startTask();
                return true;

            }
            catch (Exception)
            {
                Debug.WriteLine("PCP Exception: Can not move the PP");
            }
            return false;
        }

        // Serialize a Rapid data array and save it
        public void serializeAndSave(string fileName, RapidData[] data)
        {
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(fileName);

                for(int i = 0; i < data.Length; i++)
                    sw.WriteLine(data[i].StringValue);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }

        // Deserialize a file into a Rapid data array
        public void deserializeAndLoad(string fileName, ref RapidData[] data)
        {
            string line;
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(fileName);

                //Read the first line of text
                line = sr.ReadLine();

                //Continue to read until you reach end of file
                int i = 0;
                while (line != null && i < data.Length)
                {
                    data[i] = default(RapidData);
                    data[i].StringValue = line;
                    line = sr.ReadLine();
                }

                //close the file
                sr.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }

		public void runController(int id, ControllerInfoCollection controllers, SimuYumiController controller)
		{
			ControllerInfo controllerInfo = controllers[id];
			Console.WriteLine("Choose: " + controllerInfo.IPAddress.ToString());
			controller.connectRobot(controllerInfo);
			Console.WriteLine("Controller connected to robot");
			string serverAddr = null;
			if (IsVirtual)
			{
				serverAddr = "tcp://*:5554";
				Console.WriteLine("serverAddr:" +  serverAddr);
			}
			else
			{
				serverAddr = "tcp://*:5555";
				Console.WriteLine("serverAddr:" + serverAddr);
			}
			SimuYumiNode writer = new SimuYumiNode(controller, controllerInfo, serverAddr, "tcp://127.0.0.1:5556");
			Console.WriteLine("SimuYumiNode run");

			ThreadStart nodeRef = new ThreadStart(writer.run);
			Thread nodeThread = new Thread(nodeRef);
			nodeThread.Start();
		}

        // Main test function
        public static void Main(string[] args)
        {
            NetworkScanner scanner = new NetworkScanner();
            scanner.Scan();
            ControllerInfoCollection controllers = scanner.Controllers;
			ControllerInfo controllerInfo;

			Console.WriteLine("Scanning for controllers!");

			int controllerNum = controllers.Count();

			Console.WriteLine("Controller List: ");
			for (int i = 0; i< controllerNum; ++i)
			{
				controllerInfo = controllers[i];				
				Console.WriteLine(i.ToString() + ": " + controllerInfo.IPAddress.ToString());
			}
			Console.WriteLine("all: Connect all controller, please run the coordinateur");

			string choice = Console.ReadLine();
			if (choice.Equals("0"))
			{
				SimuYumiController controller = new SimuYumiController("T_ROB_R", "MainSimuModule");
				controller.runController(0, controllers, controller);
			}
			else if (choice.Equals("1"))
			{
				SimuYumiController controller = new SimuYumiController("T_ROB_R", "MainSimuModule");
				controller.runController(1, controllers, controller);
			}
			else if(choice.Equals("all"))
			{
				for (int i = 0; i < controllerNum; ++i)
				{
					SimuYumiController controller = new SimuYumiController("T_ROB_R", "MainSimuModule");
					controller.runController(i, controllers, controller);
				}
			}
			else
			{
				Console.WriteLine("Unkown choice");
			}
			/*SimuYumiController controller = new SimuYumiController("T_ROB_R", "MainSimuModule");
            controller.connectRobot(controllerInfo);

            Console.WriteLine("Controller connected to robot");
			SimuYumiNode writer = new SimuYumiNode(controller, controllerInfo, "tcp://*:5551", "tcp://127.0.0.1:5556");
			Console.WriteLine("SimuYumiNode run");
			writer.run();*/
			Console.ReadKey();

			//controller.controller.FileSystem.PutFile("C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.json", "motion2.json");

			/*string fileName = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\example_traj.data";
            string outFile = "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\example_traj_out.data";

            float[,] path = new float[253, 7];
            controller.deserializeTrajAndLoad(fileName, ref path); //read stream from fileName and save in path
            Console.WriteLine("Path has a length of " + path.GetLength(0));
            Console.WriteLine(path[252, 6]);
            controller.serializePathAndSave(outFile, path, false); //get data from path and save in outFile
            // Reading values as RawBytes*/

			/*
            Stopwatch watch = new Stopwatch();
            watch.Start();
            controller.controller.FileSystem.FileExists("motion2.json");
            controller.controller.FileSystem.PutFile("C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.json","motion2.json");
            controller.controller.FileSystem.GetFile("motion2.json", "C:\\Users\\Master_2013_2\\Documents\\yumi_control\\test_motion2.copy.json");
            controller.controller.FileSystem.RemoveFile("motion2.json");
            watch.Stop();
            Console.WriteLine("Took " + watch.ElapsedMilliseconds + " ms");
            */
		}

    }
}
