﻿using System;

namespace YumiControlPC
{
    partial class YumiController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controllerList = new System.Windows.Forms.ListView();
            this.IPAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Availability = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Virtual = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SystemName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RWVersion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.controllerNname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.connectButton = new System.Windows.Forms.Button();
            this.softStopButton = new System.Windows.Forms.Button();
            this.logsButton = new System.Windows.Forms.Button();
            this.runButton = new System.Windows.Forms.Button();
            this.logsBox = new System.Windows.Forms.TextBox();
            this.taskBox = new System.Windows.Forms.TextBox();
            this.taskLabel = new System.Windows.Forms.Label();
            this.moduleLabel = new System.Windows.Forms.Label();
            this.moduleBox = new System.Windows.Forms.TextBox();
            this.stateLabel = new System.Windows.Forms.Label();
            this.userBox = new System.Windows.Forms.TextBox();
            this.userLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.passwordBox = new System.Windows.Forms.TextBox();
            this.anonymousBox = new System.Windows.Forms.CheckBox();
            this.pauseRobotButton = new System.Windows.Forms.Button();
            this.serverButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // controllerList
            // 
            this.controllerList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IPAddress,
            this.ID,
            this.Availability,
            this.Virtual,
            this.SystemName,
            this.RWVersion,
            this.controllerNname});
            this.controllerList.FullRowSelect = true;
            this.controllerList.GridLines = true;
            this.controllerList.Location = new System.Drawing.Point(13, 13);
            this.controllerList.Name = "controllerList";
            this.controllerList.Size = new System.Drawing.Size(672, 335);
            this.controllerList.TabIndex = 0;
            this.controllerList.UseCompatibleStateImageBehavior = false;
            this.controllerList.View = System.Windows.Forms.View.Details;
            this.controllerList.SelectedIndexChanged += new System.EventHandler(this.controllerList_SelectedIndexChanged);
            // 
            // IPAddress
            // 
            this.IPAddress.Text = "IP Address";
            this.IPAddress.Width = 80;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 126;
            // 
            // Availability
            // 
            this.Availability.Text = "Availability";
            this.Availability.Width = 67;
            // 
            // Virtual
            // 
            this.Virtual.Text = "Virtual";
            this.Virtual.Width = 49;
            // 
            // SystemName
            // 
            this.SystemName.Text = "System name";
            this.SystemName.Width = 111;
            // 
            // RWVersion
            // 
            this.RWVersion.Text = "RW Version";
            this.RWVersion.Width = 77;
            // 
            // controllerNname
            // 
            this.controllerNname.Text = "Controller name";
            this.controllerNname.Width = 158;
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Location = new System.Drawing.Point(13, 354);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(94, 30);
            this.connectButton.TabIndex = 1;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // softStopButton
            // 
            this.softStopButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.softStopButton.Location = new System.Drawing.Point(396, 354);
            this.softStopButton.Name = "softStopButton";
            this.softStopButton.Size = new System.Drawing.Size(84, 30);
            this.softStopButton.TabIndex = 4;
            this.softStopButton.Text = "Soft Stop";
            this.softStopButton.UseVisualStyleBackColor = true;
            this.softStopButton.Click += new System.EventHandler(this.softStopButton_Click);
            // 
            // logsButton
            // 
            this.logsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logsButton.Location = new System.Drawing.Point(486, 354);
            this.logsButton.Name = "logsButton";
            this.logsButton.Size = new System.Drawing.Size(101, 30);
            this.logsButton.TabIndex = 5;
            this.logsButton.Text = "Toggle Logs";
            this.logsButton.UseVisualStyleBackColor = true;
            this.logsButton.Click += new System.EventHandler(this.logsButton_Click);
            // 
            // runButton
            // 
            this.runButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runButton.Location = new System.Drawing.Point(593, 354);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(92, 30);
            this.runButton.TabIndex = 6;
            this.runButton.Text = "Run Testfile";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // logsBox
            // 
            this.logsBox.Enabled = false;
            this.logsBox.Location = new System.Drawing.Point(13, 452);
            this.logsBox.Multiline = true;
            this.logsBox.Name = "logsBox";
            this.logsBox.Size = new System.Drawing.Size(672, 78);
            this.logsBox.TabIndex = 8;
            // 
            // taskBox
            // 
            this.taskBox.Location = new System.Drawing.Point(49, 394);
            this.taskBox.Name = "taskBox";
            this.taskBox.Size = new System.Drawing.Size(96, 20);
            this.taskBox.TabIndex = 9;
            this.taskBox.Text = "T_ROB_R";
            // 
            // taskLabel
            // 
            this.taskLabel.AutoSize = true;
            this.taskLabel.Location = new System.Drawing.Point(12, 397);
            this.taskLabel.Name = "taskLabel";
            this.taskLabel.Size = new System.Drawing.Size(31, 13);
            this.taskLabel.TabIndex = 10;
            this.taskLabel.Text = "Task";
            // 
            // moduleLabel
            // 
            this.moduleLabel.AutoSize = true;
            this.moduleLabel.Location = new System.Drawing.Point(151, 397);
            this.moduleLabel.Name = "moduleLabel";
            this.moduleLabel.Size = new System.Drawing.Size(42, 13);
            this.moduleLabel.TabIndex = 11;
            this.moduleLabel.Text = "Module";
            // 
            // moduleBox
            // 
            this.moduleBox.Location = new System.Drawing.Point(208, 394);
            this.moduleBox.Name = "moduleBox";
            this.moduleBox.Size = new System.Drawing.Size(100, 20);
            this.moduleBox.TabIndex = 12;
            this.moduleBox.Text = "MainSimuModule";
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Location = new System.Drawing.Point(457, 397);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(37, 13);
            this.stateLabel.TabIndex = 13;
            this.stateLabel.Text = "Offline";
            // 
            // userBox
            // 
            this.userBox.Location = new System.Drawing.Point(49, 420);
            this.userBox.Name = "userBox";
            this.userBox.Size = new System.Drawing.Size(96, 20);
            this.userBox.TabIndex = 14;
            // 
            // userLabel
            // 
            this.userLabel.AutoSize = true;
            this.userLabel.Location = new System.Drawing.Point(12, 423);
            this.userLabel.Name = "userLabel";
            this.userLabel.Size = new System.Drawing.Size(29, 13);
            this.userLabel.TabIndex = 15;
            this.userLabel.Text = "User";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(151, 423);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(53, 13);
            this.passwordLabel.TabIndex = 16;
            this.passwordLabel.Text = "Password";
            // 
            // passwordBox
            // 
            this.passwordBox.Location = new System.Drawing.Point(208, 420);
            this.passwordBox.Name = "passwordBox";
            this.passwordBox.PasswordChar = '*';
            this.passwordBox.Size = new System.Drawing.Size(100, 20);
            this.passwordBox.TabIndex = 17;
            // 
            // anonymousBox
            // 
            this.anonymousBox.AutoSize = true;
            this.anonymousBox.Location = new System.Drawing.Point(324, 396);
            this.anonymousBox.Name = "anonymousBox";
            this.anonymousBox.Size = new System.Drawing.Size(113, 17);
            this.anonymousBox.TabIndex = 19;
            this.anonymousBox.Text = "Anonymous user ?";
            this.anonymousBox.UseVisualStyleBackColor = true;
            this.anonymousBox.CheckedChanged += new System.EventHandler(this.anonymousBox_CheckedChanged);
            // 
            // pauseRobotButton
            // 
            this.pauseRobotButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pauseRobotButton.Location = new System.Drawing.Point(113, 354);
            this.pauseRobotButton.Name = "pauseRobotButton";
            this.pauseRobotButton.Size = new System.Drawing.Size(109, 30);
            this.pauseRobotButton.TabIndex = 20;
            this.pauseRobotButton.Text = "Pause Robot";
            this.pauseRobotButton.UseVisualStyleBackColor = true;
            this.pauseRobotButton.Click += new System.EventHandler(this.pauseRobotButton_Click);
            // 
            // serverButton
            // 
            this.serverButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverButton.Location = new System.Drawing.Point(228, 354);
            this.serverButton.Name = "serverButton";
            this.serverButton.Size = new System.Drawing.Size(94, 30);
            this.serverButton.TabIndex = 21;
            this.serverButton.Text = "Run server";
            this.serverButton.UseVisualStyleBackColor = true;
            this.serverButton.Click += new System.EventHandler(this.serverButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Location = new System.Drawing.Point(329, 354);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(61, 30);
            this.cancelButton.TabIndex = 22;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // YumiController
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 542);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.serverButton);
            this.Controls.Add(this.pauseRobotButton);
            this.Controls.Add(this.anonymousBox);
            this.Controls.Add(this.passwordBox);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.userLabel);
            this.Controls.Add(this.userBox);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.moduleBox);
            this.Controls.Add(this.moduleLabel);
            this.Controls.Add(this.taskLabel);
            this.Controls.Add(this.taskBox);
            this.Controls.Add(this.logsBox);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.logsButton);
            this.Controls.Add(this.softStopButton);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.controllerList);
            this.Name = "YumiController";
            this.Text = "YumiController";
            this.Load += new System.EventHandler(this.YumiController_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView controllerList;
        private System.Windows.Forms.ColumnHeader IPAddress;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader Availability;
        private System.Windows.Forms.ColumnHeader Virtual;
        private System.Windows.Forms.ColumnHeader SystemName;
        private System.Windows.Forms.ColumnHeader RWVersion;
        private System.Windows.Forms.ColumnHeader controllerNname;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button softStopButton;
        private System.Windows.Forms.Button logsButton;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox logsBox;
        private System.Windows.Forms.TextBox taskBox;
        private System.Windows.Forms.Label taskLabel;
        private System.Windows.Forms.Label moduleLabel;
        private System.Windows.Forms.TextBox moduleBox;
        private System.Windows.Forms.Label stateLabel;
        private System.Windows.Forms.TextBox userBox;
        private System.Windows.Forms.Label userLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.TextBox passwordBox;
        private System.Windows.Forms.CheckBox anonymousBox;
        private System.Windows.Forms.Button pauseRobotButton;
        private System.Windows.Forms.Button serverButton;
        private System.Windows.Forms.Button cancelButton;
    }
}