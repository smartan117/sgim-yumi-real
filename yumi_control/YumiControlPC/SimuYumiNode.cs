﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;
using ABB.Robotics.Controllers;

namespace YumiControlPC
{
    public class SimuYumiNode
    {
        private int orderRcv = 0;
        private string srv;
        private string rstSrv;

        private ResponseSocket srv_resp = null;  // Server communicating with external programs, awaiting commands
        private RequestSocket srv_req = null;  // Client communicating with Robotstudio simulation to reset robot

		//private ResponseSocket clt_resp = null;  // Server communicating with external programs, awaiting commands
		private RequestSocket clt_req = null;  // Client communicating with real robot

		private SimuYumiController controller;  // Hand-made controller of Yumi with high-level commands
        private dynamic currentOrder = null;
        public bool working = true;
		private bool IsVirtual = true;


		public SimuYumiNode(SimuYumiController controller, ControllerInfo ctrlInfo, string srv, string rstSrv)
        {
            this.srv = srv;
            this.rstSrv = rstSrv;
            this.controller = controller;
			IsVirtual = ctrlInfo.IsVirtual;
		}

        // Check if message has correct formatting and deserialize it
        private void analyzeMsg(string msg)
        {
            currentOrder = null;
            try
            {
                dynamic dico = JsonConvert.DeserializeObject(msg);
                if (dico.type is Object && dico.payload is Object && (string)dico.type is string)
                    currentOrder = dico;
            }
            catch (Exception)
            {

            }
        }

        // Apply the order received and prepare acknowledgement message
        private void applyOrder(ref dynamic ack)
        {
            if (currentOrder == null || controller == null)
                return;
            string order = currentOrder.type;
            ack = new ExpandoObject();
            ack.type = order;
            Debug.WriteLine("Order received: " + orderRcv);
            orderRcv += 1;
            try
            {
				if (string.Compare(order, "move") == 0)
				{
					// Received a policy execution order
					float[,] path = currentOrder.payload.path.ToObject<float[,]>();
					
					// currentOrder: json convert
					float dt = currentOrder.payload.dt;
					float[] pose = new float[7];
					float[] joints = new float[7];
					int exeTime = 0;
					bool ok = controller.moveRobotEasy(path, dt, ref pose, ref joints, ref exeTime, false);
					ack.payload = new ExpandoObject();
					ack.payload.pose = pose;
					ack.payload.joints = joints;
					ack.payload.exeTime = exeTime;
					if (IsVirtual)
					{
						ack.from = 3;
					}
					else
					{
						ack.from = 4;
					}					
					ack.valid = ok;
                }
                else if (string.Compare(order, "init") == 0)
                {
                    // Received a joints GoTo Init Position order
                    float[] goal = currentOrder.payload.joints.ToObject<float[]>();
                    float t = currentOrder.payload.dt;
                    ack.valid = controller.goToInitPosition(goal, t);
                }
                else if (string.Compare(order, "reset") == 0)
                {
					//Init payload
					ack.payload = new ExpandoObject();					
					if (IsVirtual)
					{
						ack.from = 3;
					}
					else
					{
						ack.from = 4;
					}

					// Received the order to reset the environmental setup
					if (controller.stopTask())
                    {
                        while (controller.isRunning())
                            Thread.Sleep(50);
						float dt = currentOrder.payload.dt;
						int dsNumerator = currentOrder.payload.dsNumerator;
						int dsDenominator = currentOrder.payload.dsDenominator;
						int exeTime = 0;
						bool addinAckValid = true;

						if (IsVirtual)
						{							
							double[] joints = currentOrder.payload.joints.ToObject<double[]>();
							dynamic msg = new ExpandoObject();
							msg.type = "reset";
							msg.payload = joints;
							srv_req.SendFrame(JsonConvert.SerializeObject((ExpandoObject)msg));
							string answer = srv_req.ReceiveFrameString();
							// Attention: currentOrder is changed in the analyzeMsg()
							try
							{
								dynamic dico = JsonConvert.DeserializeObject(answer);
								if (dico == null)
									return;
								if (dico.type is Object && dico.payload is Object && (string)dico.type is string)
									addinAckValid = dico.valid;
							}
							catch (Exception)
							{

							}
						}
						
						if ((addinAckValid || !IsVirtual) && controller.resetRobot(dt, dsNumerator, dsDenominator, ref exeTime))
						{
							while (controller.isRunning())
								Thread.Sleep(50);

							ack.valid = true;
							if (!IsVirtual)
							{
								ack.payload.exeTime = exeTime;
							}
							Debug.WriteLine("reset robot succeed");
						}
						else
						{
							ack.valid = false;
							Debug.WriteLine("reset robot failed");
						}
					}
                    else
                    {
                        ack.payload = "";
                        ack.valid = false;
						Debug.WriteLine("reset stop task failed");
					}
                }
                else if (string.Compare(order, "reset_js") == 0)
				{
                    // Received the order to reset the environmental setup
                    ack.payload = new ExpandoObject();
                    if (controller.stopTask())
					{
						while (controller.isRunning())
							Thread.Sleep(50);

                        // currentOrder: json convert
                        float dt = currentOrder.payload.dt;
                        int dsNumerator = currentOrder.payload.dsNumerator;
                        int dsDenominator = currentOrder.payload.dsDenominator;
                        int exeTime = 0;

                        if (currentOrder != null && controller.resetRobot_js(dt, dsNumerator, dsDenominator, ref exeTime))
                        {
                            while (controller.isRunning())
								Thread.Sleep(50);
                            ack.payload.exeTime = exeTime;
                            ack.valid = true;
                        }
						else
                        {
                            ack.valid = false;
                            ack.payload = "";
                            Debug.WriteLine("reset robot failed");
                        }                       

                    }
					else
					{
						ack.payload = "";
						ack.valid = false;
                        Debug.WriteLine("reset stop task failed");
                    }
				}
            }
            catch (Exception) { }
        }

        public void ackToReq(ResponseSocket resp)
        {
            /*Debug.WriteLine((string)currentOrder.type);
            Debug.WriteLine(currentOrder.payload);
            Debug.WriteLine((string)currentOrder.valid);*/
            dynamic ack = new ExpandoObject();
            ack.type = (string)currentOrder.type;
            ack.valid = true;
			//ack.from = currentOrder.from;
			Debug.WriteLine("ACK to python" + JsonConvert.SerializeObject((ExpandoObject)ack));
            resp.SendFrame(JsonConvert.SerializeObject((ExpandoObject)ack));
        }

        // Thread main function
        public void run()
        {
			srv_resp = new ResponseSocket();
			try
			{
				srv_resp.Bind(srv);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
			Debug.WriteLine("Server online!");


			//srv_req is used for the reset addin
			if (IsVirtual)
			{
				srv_req = new RequestSocket();
				srv_req.Connect(rstSrv);
				Debug.WriteLine("Server for addin online!");
			}

			clt_req = new RequestSocket();
			clt_req.Connect("tcp://127.0.0.1:5551");
			Debug.WriteLine("Client Writer online!");

			while (working)
            {
                string msg = srv_resp.ReceiveFrameString();
                Debug.WriteLine("order received from python" + msg);
                analyzeMsg(msg);
                ackToReq(srv_resp);				

				dynamic rsp = null;
                applyOrder(ref rsp);
                if (rsp == null)
                {
					rsp = new ExpandoObject();
					rsp.type = "error";
					rsp.payload = msg;
					rsp.valid = false;
                }
				clt_req.SendFrame(JsonConvert.SerializeObject((ExpandoObject)rsp));
				Debug.WriteLine("Response to python is : " + JsonConvert.SerializeObject((ExpandoObject)rsp));
				string clt_rsp = clt_req.ReceiveFrameString();
				Debug.WriteLine("ACK From python: " + clt_rsp);
				//resp.SendFrame(JsonConvert.SerializeObject((ExpandoObject)rsp));
			}
			srv_resp.Dispose();
            Debug.WriteLine("Writer offline!");
        }

    }
}
