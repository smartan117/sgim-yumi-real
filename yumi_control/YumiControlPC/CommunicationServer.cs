﻿using System;
using System.Collections.Generic;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

using YumiControlPC;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Diagnostics;

public class CommunicationServer
{
    private string srv;
    public string portPuller;
    public string portPub;
    private bool running;
    private PublisherSocket pub;
    private PullSocket puller;
    private YumiLowLevelController robot;
    private dynamic currentOrder;

    private static string[] ORDERS = { "setPosePath", "setPose", "setJoints", "setJointsPath", "getPose", "getJoints", "cancel", "softStop", "suspend", "exit", "setCollision" };

    public CommunicationServer(YumiLowLevelController robot, string srv, string portPuller = "5555", string portPub = "5556")
	{
        this.srv = srv;
        this.portPub = portPub;
        this.portPuller = portPuller;
        this.robot = robot;
        pub = null;
        puller = null;
        running = true;
	}

    private void analyzeMsg(string msg)
    {
        currentOrder = null;
        try
        {
            dynamic dico = JsonConvert.DeserializeObject(msg);
            if(dico.type is Object && dico.payload is Object && (string)dico.type is string)
                currentOrder = dico;
        }
        catch (Exception)
        {
            
        }
    }
    
    private void postOrder(ref Dictionary<string, string> ack) //DIRTY: post order but pre-ack
    {
        string order = currentOrder.type;
        ack.Add("type", (string)currentOrder.type);
        
        if (string.Compare(order, "suspend") == 0)
        {
            try
            {
                bool realVal = true;
                bool read = robot.readRunning(ref realVal);
                ack.Add("payload", JsonConvert.SerializeObject(realVal));
            }
            catch (Exception)
            {
                ack.Add("payload", JsonConvert.SerializeObject(false));
            }
        }
        else if (string.Compare(order, "softStop") == 0)
        {
            try
            {
                bool realVal = true;
                bool read = robot.readSoftStop(ref realVal);
                ack.Add("payload", JsonConvert.SerializeObject(realVal));
            }
            catch (Exception)
            {
                ack.Add("payload", JsonConvert.SerializeObject(false));
            }
        }
        else if (string.Compare(order, "setCollision") == 0) { 
}
        else
        {
            ack.Add("payload", JsonConvert.SerializeObject(true));
        }
    }

    private bool applyOrder()
    {
        if (currentOrder == null || robot == null)
            return false;
        string order = currentOrder.type;
        if(string.Compare(order, "setPose") == 0)
        {
            try
            {
                float[] pose = currentOrder.payload.pose.ToObject<float[]>();
                float duration = currentOrder.payload.duration;

                return robot.sendPoseOrder(pose, duration);
            }
            catch (Exception) { }
        }
        else if(string.Compare(order, "setJoints") == 0)
        {
            try
            {
                float[] joints = currentOrder.payload.joints.ToObject<float[]>();
                float duration = currentOrder.payload.duration;

                return robot.sendJointOrder(joints, duration);
            }
            catch (Exception) { }
        }
        else if (string.Compare(order, "setPosePath") == 0)
        {
            try
            {
                float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                float duration = currentOrder.payload.duration;

                return robot.sendPosePathOrder(path, duration);
            }
            catch (Exception) { }
        }
        else if (string.Compare(order, "setJointsPath") == 0)
        {
            try
            {
                float[,] path = currentOrder.payload.path.ToObject<float[,]>();
                float duration = currentOrder.payload.duration;

                return robot.sendJointPathOrder(path, duration);
            }
            catch (Exception) { }
        }
        else if (string.Compare(order, "cancel") == 0)
        {
            return robot.cancelOrder();
        }
        else if (string.Compare(order, "suspend") == 0)
        {
            try
            {
                bool value = currentOrder.payload;
                return robot.suspendToggle(value);
            }
            catch (Exception) { }
        }
        else if (string.Compare(order, "softStop") == 0)
        {
            try
            {
                bool value = currentOrder.payload;
                return robot.softStop(value);
            }
            catch (Exception) { }
        }
        else if(string.Compare(order, "setCollision") == 0)
        {
            try
            {
                bool value = currentOrder.payload;
                return robot.setCollisionAvoidance(value);
            }
            catch (Exception) { }
        }
        else if (string.Compare(order, "exit") == 0)
        {
            running = false;
            return true;
        }
        return false;
    }

    public void run()
    {
        /*using (var server = new ResponseSocket(srv))
        {
            Console.WriteLine("Server bound. Waiting for client request.");

            while(running)
            {
                string msg = server.ReceiveFrameString();
                server.SendFrame("Hi there!");
            }
        }
        */
        try
        {
            puller = new PullSocket();
            puller.Bind(srv + ":" + portPuller);
            pub = new PublisherSocket();
            pub.Bind(srv + ":" + portPub);
            Debug.WriteLine("Server online!");

            while (running)
            {
                string msg = puller.ReceiveFrameString();
                Debug.WriteLine("Controller received: " + msg);
                analyzeMsg(msg);
                bool valid = applyOrder();
                Dictionary<string, string> ack = new Dictionary<string, string>();
                if (currentOrder == null)
                {
                    ack.Add("type", "error");
                    ack.Add("payload", msg);
                }
                else if (!valid)
                {
                    ack.Add("type", (string) currentOrder.type);
                    ack.Add("payload", JsonConvert.SerializeObject(valid));
                }
                else
                {
                    postOrder(ref ack);
                }
                pub.SendMoreFrame("state").SendFrame(JsonConvert.SerializeObject(ack));
                Debug.WriteLine("Controller sent: " + JsonConvert.SerializeObject(ack));
            }

            Debug.WriteLine("Server stopped!");
        }
        catch(Exception e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            puller.Dispose();
            pub.Dispose();
        }
    }

    static void Main(string[] args)
    {
        CommunicationServer server = new CommunicationServer(null, "tcp://*");
        server.run();
        Console.ReadKey();
    }

}
